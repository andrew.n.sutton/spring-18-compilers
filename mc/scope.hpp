#pragma once

#include "symbol.hpp"

#include <cassert>
#include <unordered_map>

class decl;


/// A scope is a region of source code for one or more declaration. Each
/// scope is inherently linked to its enclosing scope.
struct scope 
  : std::unordered_map<symbol, decl*>
{
  scope(scope* p = nullptr)
    : parent(p)
  { }
  
  virtual ~scope() = default;

  /// Returns the declaration of a name, or null if it doesn't exist.
  decl* lookup(symbol sym) const 
  {
    auto iter = find(sym);
    return iter == end() ? nullptr : iter->second;
  }

  /// Declares sym in the current scope.
  void declare(symbol sym, decl* d) 
  {
    assert(count(sym) == 0);
    emplace(sym, d);
  }

  /// The parent scope.
  scope* parent;
};

/// Represents the scope of declarations not inside a function definition.
struct global_scope : scope 
{
  using scope::scope;
};

/// Represents the scope of declarations inside a function parameter list.
struct parameter_scope : scope
{
  using scope::scope;
};

/// Represents the scope of declarations inside a block statement.
///
/// \todo Add information needed to associate break and continue statements
/// with loops.
struct block_scope : scope
{
  using scope::scope;
};
