#pragma once

#include "token.hpp"

class type;
class expr;
class stmt;
class decl;
class fn_decl;

using type_list = std::vector<type*>;
using expr_list = std::vector<expr*>;
using stmt_list = std::vector<stmt*>;
using decl_list = std::vector<decl*>;

class scope;

class semantics
{
public:
  semantics();
  ~semantics();

  type* on_basic_type(token tok);
  type* on_nested_type(const type_list& ts);
  type* on_function_type(const type_list& ts, type* t);
  type* on_pointer_type(type* t);
  type* on_reference_type(type* t);

  expr* on_assignment_expression(expr* e1, expr* e2);
  expr* on_conditional_expression(expr* e1, expr* e2, expr* e3);
  expr* on_logical_or_expression(expr* e1, expr* e2);
  expr* on_logical_and_expression(expr* e1, expr* e2);
  expr* on_bitwise_or_expression(expr* e1, expr* e2);
  expr* on_bitwise_xor_expression(expr* e1, expr* e2);
  expr* on_bitwise_and_expression(expr* e1, expr* e2);
  expr* on_equality_expression(token tok, expr* e1, expr* e2);
  expr* on_relational_expression(token tok, expr* e1, expr* e2);
  expr* on_shift_expression(token tok, expr* e1, expr* e2);
  expr* on_additive_expression(token tok, expr* e1, expr* e2);
  expr* on_multiplicative_expression(token tok, expr* e1, expr* e2);
  expr* on_cast_expression(expr* e, type* t);
  expr* on_unary_expression(token tok, expr* e);
  expr* on_call_expression(expr* e, const expr_list& args);
  expr* on_index_expression(expr* e, const expr_list& args);
  expr* on_id_expression(token tok);
  expr* on_integer_literal(token tok);
  expr* on_boolean_literal(token tok);
  expr* on_float_literal(token tok);

  stmt* on_block_statement(const stmt_list& ss);
  void start_block();
  void finish_block();
  stmt* on_if_statement(expr* e, stmt* s1, stmt* s2);
  stmt* on_while_statement(expr* e, stmt* s);
  stmt* on_break_statement();
  stmt* on_continue_statement();
  stmt* on_return_statement(expr* e);
  stmt* on_declaration_statement(decl* d);
  stmt* on_expression_statement(expr* e);

  decl* on_variable_declaration(token n, type* t);
  decl* on_variable_definition(decl*, expr* e);
  decl* on_constant_declaration(token n, type* t);
  decl* on_constant_definition(decl*, expr* e);
  decl* on_value_declaration(token n, type* t);
  decl* on_value_definition(decl*, expr* e);
  decl* on_parameter_declaration(token n, type* t);
  decl* on_function_declaration(token n, const decl_list& ps, type* t);
  decl* on_function_definition(decl* d, stmt* s);

  decl* on_program(const decl_list& ds);

  // Scope

  void enter_global_scope();
  void enter_parameter_scope();
  void enter_block_scope();
  void leave_scope();
  scope* get_current_scope() const { return m_scope; }

  // Context

  fn_decl* get_current_function() const { return m_fn; }

  // Declaration

  void declare(decl* d);

  // Name lookup

  decl* lookup(symbol n);

  // Type checking
  
  expr* require_reference(expr* e);
  expr* require_value(expr* e);
  expr* require_integer(expr* e);
  expr* require_boolean(expr* e);
  expr* require_function(expr* e);
  expr* require_arithmetic(expr* e);
  expr* require_numeric(expr* e);
  expr* require_scalar(expr* e);
  expr* require_pointer(expr* e);
  
  type* require_same(type* t1, type* t2);

  type* common_type(type* t1, type* t2);
  
  // Conversion

  expr* convert_to_value(expr* e);
  expr* convert_to_bool(expr* e);
  expr* convert_to_char(expr* e);
  expr* convert_to_int(expr* e);
  expr* convert_to_float(expr* e);
  expr* convert_to_type(expr* e, type* t);

private:
  /// The current scope.
  scope* m_scope;

  /// The current function.
  fn_decl* m_fn; 

  /// Builtin types
  type* m_bool;
  type* m_char;
  type* m_int;
  type* m_float;
};

