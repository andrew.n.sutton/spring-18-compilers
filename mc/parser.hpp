#pragma once

#include "lexer.hpp"
#include "semantics.hpp"

#include <deque>
#include <vector>

class type;
class expr;
class stmt;
class decl;
class prog;

using type_list = std::vector<type*>;
using expr_list = std::vector<expr*>;
using stmt_list = std::vector<stmt*>;
using decl_list = std::vector<decl*>;


class parser
{
public:
  parser(symbol_table& syms, const file& f);

  type* parse_type();
  type* parse_basic_type();
  type* parse_pointer_type();
  type* parse_reference_type();
  type_list parse_type_list();

  
  expr* parse_expression();
  expr* parse_assignment_expression();
  expr* parse_conditional_expression();
  expr* parse_logical_or_expression();
  expr* parse_logical_and_expression();
  expr* parse_bitwise_or_expression();
  expr* parse_bitwise_xor_expression();
  expr* parse_bitwise_and_expression();
  expr* parse_equality_expression();
  expr* parse_relational_expression();
  expr* parse_shift_expression();
  expr* parse_additive_expression();
  expr* parse_multiplicative_expression();
  expr* parse_cast_expression();
  expr* parse_unary_expression();
  expr* parse_postfix_expression();
  expr* parse_primary_expression();
  expr_list parse_argument_list();
  
  stmt* parse_statement();
  stmt* parse_block_statement();
  stmt* parse_if_statement();
  stmt* parse_while_statement();
  stmt* parse_break_statement();
  stmt* parse_continue_statement();
  stmt* parse_return_statement();
  stmt* parse_declaration_statement();
  stmt* parse_expression_statement();
  stmt_list parse_statement_seq();

  decl* parse_declaration();
  decl* parse_local_declaration();
  decl* parse_object_definition();
  decl* parse_variable_definition();
  decl* parse_constant_definition();
  decl* parse_value_definition();
  decl* parse_function_definition();
  decl* parse_parameter();
  decl_list parse_parameter_list();
  decl_list parse_parameter_clause();
  decl_list parse_declaration_seq();

  decl* parse_program();

private:
  token_name lookahead();
  token_name lookahead(int n);
  token match(token_name n);
  token match_if(token_name n);
  token match_if_logical_or();
  token match_if_logical_and();
  token match_if_bitwise_or();
  token match_if_bitwise_xor();
  token match_if_bitwise_and();
  token match_if_equality();
  token match_if_relational();
  token match_if_shift();
  token match_if_additive();
  token match_if_multiplicative();
  token match_if_reference_glyph();
  token match_if_pointer_glyph();

  token accept();
  token peek();
  void fetch();

private:
  lexer m_lex;
  
  // The current (lookahead) token and any that have
  // been scanned beyond that.
  std::deque<token> m_tok; 

  // The semantic actions of the compiler. This
  // defines the translation scheme.
  semantics m_act;
};

inline
parser::parser(symbol_table& syms, const file& f)
  : m_lex(syms, f), m_tok()
{
  fetch();
}
