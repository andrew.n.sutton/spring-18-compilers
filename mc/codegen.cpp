#include "codegen.hpp"
#include "type.hpp"
#include "expr.hpp"
#include "decl.hpp"
#include "stmt.hpp"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Constant.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Support/raw_ostream.h>

#include <cassert>
#include <iostream>
#include <sstream>
#include <unordered_map>

/// Associates declarations with values.
using variable_map = std::unordered_map<const decl*, llvm::Value*>;

// -------------------------------------------------------------------------- //
// Root context

/// The root code generation context. This provides facilities for 
/// translating Beaker types and constants into LLVM types and constants.
struct cg_context
{
  cg_context()
    : ll(new llvm::LLVMContext())
  { }

  /// Destroys the codegen context.
  ~cg_context() { delete ll; }

  /// Returns the LLVM context.
  llvm::LLVMContext *get_context() const { return ll; }

  // Names

  /// Returns a name for the declaration.
  std::string get_name(const decl* d);

  // Types

  /// Generate the corresponding type for `t`.
  llvm::Type* get_type(const type* t);
  llvm::Type* get_bool_type(const bool_type* t);
  llvm::Type* get_char_type(const char_type* t);
  llvm::Type* get_int_type(const int_type* t);
  llvm::Type* get_float_type(const float_type* t);
  llvm::Type* get_ref_type(const ref_type* t);
  llvm::Type* get_fn_type(const fn_type* t);

  /// Returns the corresponding type for the declaration `d`.
  llvm::Type* get_type(const typed_decl* d);

  /// The underlying LLVM context.
  llvm::LLVMContext* ll;
};

// -------------------------------------------------------------------------- //
// Module context

/// The module generation context provides facilities for generating 
/// and referencing module-level declarations.
struct cg_module
{
  cg_module(cg_context& cxt, const prog_decl* prog);

  /// Returns the LLVM context.
  llvm::LLVMContext* get_context() const { return parent->get_context(); }

  /// Returns the underlying LLVM module.
  llvm::Module* get_module() const { return mod; }

  //  Names

  /// Generates a declaration name for `d`.
  std::string get_name(const decl* d) { return parent->get_name(d); }

  // Types

  /// Generate a corresponding type to `t`.
  llvm::Type* get_type(const type* t) { return parent->get_type(t); }

  /// Generates a type corresponding to the type `d`.
  llvm::Type* get_type(const typed_decl* d) { return parent->get_type(d); }

  // Global values

  /// Associate the value `v` with the global declaration `d`.
  void declare(const decl* d, llvm::GlobalValue* v);

  /// Returns the global value corresponding to `d` or nullptr.
  llvm::GlobalValue* lookup(const decl* d) const;

  // Declaration generation

  /// Process expressions as top-level declarations.
  void generate();
  void generate(const decl* d);
  void generate_var_decl(const var_decl* d);
  void generate_fn_decl(const fn_decl* d);

  /// The parent context.
  cg_context* parent; 

  /// The corresponding translation unit.
  const prog_decl* prog;  
  
  /// The underlying LLVM module.
  llvm::Module* mod; 

  /// A lookup table for global modules.
  variable_map globals;
};

// -------------------------------------------------------------------------- //
// Function context

/// Provides the codegen context for expressions.
struct cg_function
{
  cg_function(cg_module& m, const fn_decl* d);

  // Context

  /// Returns the LLVM context.
  llvm::LLVMContext* get_context() const { return parent->get_context(); }

  /// Returns the owning LLVM module.
  llvm::Module* get_module() const { return parent->get_module(); }

  /// Returns the underlying LLVM Function.
  llvm::Function* get_function() const { return fn; }

  // Names

  /// Returns the name for the declaration `d`.
  std::string get_name(const decl* d) { return parent->get_name(d); }

  // Types

  /// Generates the type corresponding to the expression `e`.
  llvm::Type* get_type(const type* t) { return parent->get_type(t); }

  /// Generates the type corresponding to the expression `e`.
  llvm::Type* get_type(const expr* e) { return get_type(e->get_type()); }

  /// Generate the corresponding type for `t`.
  llvm::Type* get_type(const typed_decl* t) { return parent->get_type(t); }

  // Local variables

  /// Declare a new local value.
  void declare(const decl* x, llvm::Value* v);

  /// Lookup a value. This may return a global value.
  llvm::Value* lookup(const decl* x) const;

  // Function definition

  void define();

  // Block management

  /// Returns the entry block. This is where local variables are allocated.
  llvm::BasicBlock* get_entry_block() const { return entry; }

  /// Returns the current block.
  llvm::BasicBlock* get_current_block() const { return curr; }

  /// Returns a new block with the given name. The block is unlinked until
  /// it is emitted.
  llvm::BasicBlock* make_block(const char* label);

  /// Emits a new block, making it active.
  void emit_block(llvm::BasicBlock* bb);

  // Instruction generation

  /// Generate a list of instructions to compute the value of e.
  llvm::Value* generate_expr(const expr* e);
  llvm::Value* generate_bool_expr(const bool_expr* e);
  llvm::Value* generate_int_expr(const int_expr* e);
  llvm::Value* generate_float_expr(const float_expr* e);
  llvm::Value* generate_id_expr(const id_expr* e);
  llvm::Value* generate_unop_expr(const unop_expr* e);
  llvm::Value* generate_arithmetic_expr(const unop_expr* e);
  llvm::Value* generate_int_expr(const unop_expr* e);
  llvm::Value* generate_float_expr(const unop_expr* e);
  llvm::Value* generate_bitwise_expr(const unop_expr* e);
  llvm::Value* generate_logical_expr(const unop_expr* e);
  llvm::Value* generate_address_expr(const unop_expr* e);
  llvm::Value* generate_deref_expr(const unop_expr* e);
  llvm::Value* generate_binop_expr(const binop_expr* e);
  llvm::Value* generate_arithmetic_expr(const binop_expr* e);
  llvm::Value* generate_int_expr(const binop_expr* e);
  llvm::Value* generate_float_expr(const binop_expr* e);
  llvm::Value* generate_bitwise_expr(const binop_expr* e);
  llvm::Value* generate_logical_expr(const binop_expr* e);
  llvm::Value* generate_and_expr(const binop_expr* e);
  llvm::Value* generate_or_expr(const binop_expr* e);
  llvm::Value* generate_relational_expr(const binop_expr* e);
  llvm::Value* generate_int_relation(const binop_expr* e);
  llvm::Value* generate_fp_relation(const binop_expr* e);
  llvm::Value* generate_call_expr(const call_expr* e);
  llvm::Value* generate_index_expr(const index_expr* e);
  llvm::Value* generate_cast_expr(const cast_expr* e);
  llvm::Value* generate_cond_expr(const cond_expr* e);
  llvm::Value* generate_assign_expr(const assign_expr* e);
  llvm::Value* generate_conv_expr(const conv_expr* e);

  // Statements
  void generate_stmt(const stmt* s);
  void generate_block_stmt(const block_stmt* s);
  void generate_when_stmt(const when_stmt* s);
  void generate_if_stmt(const if_stmt* s);
  void generate_while_stmt(const while_stmt* s);
  void generate_break_stmt(const break_stmt* s);
  void generate_cont_stmt(const cont_stmt* s);
  void generate_ret_stmt(const ret_stmt* s);
  void generate_decl_stmt(const decl_stmt* s);
  void generate_expr_stmt(const expr_stmt* s);

  // Local declarations
  void generate_decl(const decl* d);
  void generate_var_decl(const var_decl* d);
  void generate_const_decl(const const_decl* d);

  /// The parent module context.
  cg_module* parent;

  /// The function original function
  const fn_decl* src;
  
  /// The underlying function being defined
  llvm::Function* fn;
  
  /// The entry block.
  llvm::BasicBlock* entry;

  /// The current block.
  llvm::BasicBlock* curr; 
  
  /// Local variables.
  variable_map locals;
};

// -------------------------------------------------------------------------- //
// Context implementation

std::string
cg_context::get_name(const decl* d)
{
  assert(d->get_name());
  return *d->get_name();
}

/// Generate the corresponding type.
llvm::Type* 
cg_context::get_type(const type* t)
{
  // Make sure we're looking at the semantic, not lexical type.
  switch (t->get_kind()) {
  case type::bool_kind:
    return get_bool_type(static_cast<const bool_type*>(t));
  case type::char_kind:
    return get_char_type(static_cast<const char_type*>(t));
  case type::int_kind:
    return get_int_type(static_cast<const int_type*>(t));
  case type::float_kind:
    return get_float_type(static_cast<const float_type*>(t));
  case type::ref_kind:
    return get_ref_type(static_cast<const ref_type*>(t));
  case type::fn_kind:
    return get_fn_type(static_cast<const fn_type*>(t));
  default:
    throw std::logic_error("invalid type");
  }
}

/// The corresponding type is i1.
llvm::Type*
cg_context::get_bool_type(const bool_type* t)
{
  return llvm::Type::getInt1Ty(*ll);
}

/// The corresponding type is i8.
llvm::Type*
cg_context::get_char_type(const char_type* t)
{
  return llvm::Type::getInt8Ty(*ll);
}

/// The corresponding type is i32.
llvm::Type*
cg_context::get_int_type(const int_type* t)
{
  return llvm::Type::getInt32Ty(*ll);
}

/// The corresponding type is float.
llvm::Type*
cg_context::get_float_type(const float_type* t)
{
  return llvm::Type::getFloatTy(*ll);
}

/// Returns a pointer to the object type.
llvm::Type*
cg_context::get_ref_type(const ref_type* t)
{
  llvm::Type* obj = get_type(t->get_object_type());
  return obj->getPointerTo();
}

/// Generate the type as a pointer. The actual function type can extracted
/// as needed for creating functions.
llvm::Type* 
cg_context::get_fn_type(const fn_type* t)
{
  const type_list& ps = t->get_parameter_types();
  std::vector<llvm::Type*> parms(ps.size());
  std::transform(ps.begin(), ps.end(), parms.begin(), [this](const type* p) {
    return get_type(p);
  });
  llvm::Type* ret = get_type(t->get_return_type());
  llvm::Type* base = llvm::FunctionType::get(ret, parms, false);
  return base->getPointerTo();
}

llvm::Type*
cg_context::get_type(const typed_decl* d)
{
  return get_type(d->get_type());
}

// -------------------------------------------------------------------------- //
// Module implementation

/// \todo Derive the name of the output file from compiler options.
cg_module::cg_module(cg_context& cxt, const prog_decl* prog)
  : parent(&cxt), 
    prog(prog), 
    mod(new llvm::Module("a.ll", *get_context()))
{ }

void
cg_module::declare(const decl* d, llvm::GlobalValue* v)
{
  assert(globals.count(d) == 0);
  globals.emplace(d, v);
}

llvm::GlobalValue*
cg_module::lookup(const decl* d) const
{
  auto iter = globals.find(d);
  if (iter != globals.end())
    return llvm::cast<llvm::GlobalValue>(iter->second);
  else
    return nullptr;
}

/// Process top-level declarations.
void 
cg_module::generate()
{
  for (const decl* d : prog->get_declarations())
    generate(d);
}

void
cg_module::generate(const decl* d)
{
  switch (d->get_kind()) {
  case decl::var_kind:
    return generate_var_decl(static_cast<const var_decl*>(d));
  
  case decl::fn_kind:
    return generate_fn_decl(static_cast<const fn_decl*>(d));

  default: 
    throw std::logic_error("invalid declaration");
  }
}

/// Generate a variable.
///
/// \todo To declare a global variable, we need to determine if it is
/// statically or dynamically initialized. A variable is statically 
/// initialized if it's initializer can be constant folded. Right now,
/// global variables are simply zero-initialized.
///
/// \todo Make a variable initialization context like we do for functions?
/// That might be pretty elegant.
void 
cg_module::generate_var_decl(const var_decl* d)
{
  std::string n = get_name(d);
  llvm::Type* t = get_type(d->get_type());
  llvm::Constant* c = llvm::Constant::getNullValue(t);
  llvm::GlobalVariable* var = new llvm::GlobalVariable(
      *mod, t, false, llvm::GlobalVariable::ExternalLinkage, c, n);

  // Create the binding.
  declare(d, var);
}

/// Generate a function from the fn expression.
void 
cg_module::generate_fn_decl(const fn_decl* d)
{
  cg_function fn(*this, d);

  // FIXME: If the function is external, then we don't define it.
  fn.define();
}

// -------------------------------------------------------------------------- //
// Function implementation

static llvm::FunctionType*
get_fn_type(llvm::Type* t)
{
  assert(llvm::isa<llvm::PointerType>(t));
  return llvm::cast<llvm::FunctionType>(t->getPointerElementType());
}

cg_function::cg_function(cg_module& m, const fn_decl* d)
  : parent(&m), src(d), fn(), entry(), curr()
{
  std::string n = get_name(d);
  llvm::Type* t = get_type(d);
  fn = llvm::Function::Create(
      get_fn_type(t), llvm::Function::ExternalLinkage, n, get_module());

  // Create a binding in the module.
  parent->declare(d, fn);

  // Build and emit the entry block.
  entry = make_block("entry");
  emit_block(entry);

  llvm::IRBuilder<> ir(get_current_block());
  
  // Configure function parameters and declare them as locals.
  assert(d->get_parameters().size() == fn->arg_size());
  auto pi = d->get_parameters().begin();
  auto ai = fn->arg_begin();
  while (ai != fn->arg_end()) {
    const parm_decl* parm = static_cast<const parm_decl*>(*pi);
    llvm::Argument& arg = *ai;

    // Configure each parameter.
    arg.setName(get_name(parm));

    // Declare local variable for each parameter and initialize it
    // with wits corresponding value.
    llvm::Value* var = ir.CreateAlloca(arg.getType(), nullptr, arg.getName());
    declare(parm, var);

    // Initialize with the value of the argument.
    llvm::IRBuilder<> ir(get_current_block());
    ir.CreateStore(&arg, var);
    
    ++ai;
    ++pi;
  }

}

void
cg_function::declare(const decl* d, llvm::Value* v)
{
  assert(locals.count(d) == 0);
  locals.emplace(d, v);
}

llvm::Value*
cg_function::lookup(const decl* d) const
{
  auto iter = locals.find(d);
  if (iter != locals.end())
    return iter->second;
  else
    return parent->lookup(d);
}

llvm::BasicBlock*
cg_function::make_block(const char* label)
{
  return llvm::BasicBlock::Create(*get_context(), label);
}

void
cg_function::emit_block(llvm::BasicBlock* bb)
{
  bb->insertInto(get_function());
  curr = bb;
}

/// Creates a return instruction for the expression.
void
cg_function::define()
{
  generate_stmt(src->get_body());
}

llvm::Value*
cg_function::generate_expr(const expr* e)
{
  switch (e->get_kind()) {
  case expr::bool_kind:
    return generate_bool_expr(static_cast<const bool_expr*>(e));
  case expr::int_kind:
    return generate_int_expr(static_cast<const int_expr*>(e));
  case expr::float_kind:
    return generate_float_expr(static_cast<const float_expr*>(e));
  case expr::id_kind:
    return generate_id_expr(static_cast<const id_expr*>(e));
  case expr::unop_kind:
    return generate_unop_expr(static_cast<const unop_expr*>(e));
  case expr::binop_kind:
    return generate_binop_expr(static_cast<const binop_expr*>(e));
  case expr::call_kind:
    return generate_call_expr(static_cast<const call_expr*>(e));
  case expr::index_kind:
    return generate_index_expr(static_cast<const index_expr*>(e));
  case expr::cond_kind:
    return generate_cond_expr(static_cast<const cond_expr*>(e));
  case expr::assign_kind:
    return generate_assign_expr(static_cast<const assign_expr*>(e));
  case expr::conv_kind:
    return generate_conv_expr(static_cast<const conv_expr*>(e));
  default: 
    throw std::runtime_error("invalid expression");
  }
}

llvm::Value*
cg_function::generate_bool_expr(const bool_expr* e)
{
  return llvm::ConstantInt::get(get_type(e), e->get_value(), false);
}

llvm::Value*
cg_function::generate_int_expr(const int_expr* e)
{
  return llvm::ConstantInt::get(get_type(e), e->get_value(), true);
}

llvm::Value*
cg_function::generate_float_expr(const float_expr* e)
{
  return llvm::ConstantFP::get(get_type(e), e->get_value());
}

llvm::Value*
cg_function::generate_id_expr(const id_expr* e)
{
  return lookup(e->get_declaration());
}

llvm::Value*
cg_function::generate_unop_expr(const unop_expr* e)
{
  switch (e->get_operator()) {
  case uo_pos:
  case uo_neg:
    return generate_arithmetic_expr(e);
  case uo_cmp:
    return generate_bitwise_expr(e);
  case uo_not:
    return generate_logical_expr(e);
  case uo_addr:
    return generate_address_expr(e);
  case uo_deref:
    return generate_deref_expr(e);
  default:
    throw std::logic_error("invalid operation");
  }
}

llvm::Value*
cg_function::generate_arithmetic_expr(const unop_expr* e)
{
  if (e->is_int())
    return generate_int_expr(e);
  else
    return generate_float_expr(e);
}

llvm::Value*
cg_function::generate_int_expr(const unop_expr* e)
{
  llvm::Value* lhs = llvm::ConstantInt::get(get_type(e), 0, true);
  llvm::Value* rhs = generate_expr(e->get_operand());
  llvm::IRBuilder<> ir(get_current_block());
  if (e->get_operator() == uo_pos)
    return ir.CreateNSWAdd(lhs, rhs);
  else
    return ir.CreateNSWSub(lhs, rhs);
}

llvm::Value*
cg_function::generate_float_expr(const unop_expr* e)
{
  llvm::Value* lhs = llvm::ConstantFP::get(get_type(e), 0.0);
  llvm::Value* rhs = generate_expr(e->get_operand());
  llvm::IRBuilder<> ir(get_current_block());
  if (e->get_operator() == uo_pos)
    return ir.CreateFAdd(lhs, rhs);
  else
    return ir.CreateFSub(lhs, rhs);
}

// Note that ~e is equivalent to -1 ^ e where -1 is an integer with all 1s set.
llvm::Value*
cg_function::generate_bitwise_expr(const unop_expr* e)
{
  llvm::Value* lhs = llvm::ConstantInt::get(get_type(e), -1, true);
  llvm::Value* rhs = generate_expr(e->get_operand());
  llvm::IRBuilder<> ir(get_current_block());
  return ir.CreateXor(lhs, rhs);
}

// Note that !e is equivalent to 1 ^ e .
llvm::Value*
cg_function::generate_logical_expr(const unop_expr* e)
{
  llvm::Value* lhs = llvm::ConstantInt::get(get_type(e), 1, true);
  llvm::Value* rhs = generate_expr(e->get_operand());
  llvm::IRBuilder<> ir(get_current_block());
  return ir.CreateFSub(lhs, rhs);
}

// Note that &e is equivalent to e. This is because e is already an address.
llvm::Value*
cg_function::generate_address_expr(const unop_expr* e)
{
  return generate_expr(e->get_operand());
}

// Note that *e is equivalent to e. This is because e is already an address.
llvm::Value*
cg_function::generate_deref_expr(const unop_expr* e)
{
  return generate_expr(e->get_operand());
}

llvm::Value*
cg_function::generate_binop_expr(const binop_expr* e)
{
  switch (e->get_operator()) {
  case bo_add:
  case bo_sub:
  case bo_mul:
  case bo_quo:
  case bo_rem:
    return generate_arithmetic_expr(e);
  case bo_and:
  case bo_ior:
  case bo_xor:
  case bo_shl:
  case bo_shr:
    return generate_bitwise_expr(e);
  case bo_land:
  case bo_lor:
    return generate_logical_expr(e);
  case bo_eq:
  case bo_ne:
  case bo_lt:
  case bo_gt:
  case bo_le:
  case bo_ge:
    return generate_relational_expr(e);
  default:
    throw std::logic_error("invalid operation");
  }
}

llvm::Value*
cg_function::generate_arithmetic_expr(const binop_expr* e)
{
  if (e->is_int())
    return generate_int_expr(e);
  else
    return generate_float_expr(e);
}

llvm::Value*
cg_function::generate_int_expr(const binop_expr* e)
{
  llvm::Value* lhs = generate_expr(e->get_lhs());
  llvm::Value* rhs = generate_expr(e->get_rhs());
  llvm::IRBuilder<> ir(get_current_block());
  switch (e->get_operator()) {
  case bo_add:
    return ir.CreateNSWAdd(lhs, rhs);
  case bo_sub:
    return ir.CreateNSWSub(lhs, rhs);
  case bo_mul:
    return ir.CreateNSWMul(lhs, rhs);
  case bo_quo:
    return ir.CreateSDiv(lhs, rhs);
  case bo_rem:
    return ir.CreateSRem(lhs, rhs);
  default:
    throw std::logic_error("invalid operator");
  }
}

llvm::Value*
cg_function::generate_float_expr(const binop_expr* e)
{
  llvm::Value* lhs = generate_expr(e->get_lhs());
  llvm::Value* rhs = generate_expr(e->get_rhs());
  llvm::IRBuilder<> ir(get_current_block());
  switch (e->get_operator()) {
  case bo_add:
    return ir.CreateFAdd(lhs, rhs);
  case bo_sub:
    return ir.CreateFSub(lhs, rhs);
  case bo_mul:
    return ir.CreateFMul(lhs, rhs);
  case bo_quo:
    return ir.CreateFDiv(lhs, rhs);
  case bo_rem:
    return ir.CreateFRem(lhs, rhs);
  default:
    throw std::logic_error("invalid operator");
  }
}

llvm::Value*
cg_function::generate_bitwise_expr(const binop_expr* e)
{
  llvm::Value* lhs = generate_expr(e->get_lhs());
  llvm::Value* rhs = generate_expr(e->get_rhs());
  llvm::IRBuilder<> ir(get_current_block());
  switch (e->get_operator()) {
  case bo_and:
    return ir.CreateAnd(lhs, rhs);
  case bo_ior:
    return ir.CreateOr(lhs, rhs);
  case bo_xor:
    return ir.CreateXor(lhs, rhs);
  case bo_shl:
    return ir.CreateShl(lhs, rhs);
  case bo_shr:
    return ir.CreateAShr(lhs, rhs);
  default:
    throw std::logic_error("invalid operator");
  }
}

llvm::Value*
cg_function::generate_logical_expr(const binop_expr* e) 
{
  if (e->get_operator() == bo_land)
    return generate_and_expr(e);
  else
    return generate_or_expr(e);
}

llvm::Value*
cg_function::generate_and_expr(const binop_expr* e)
{
  llvm::BasicBlock* lb = get_current_block();
  llvm::BasicBlock* rb = make_block("and.if");
  llvm::BasicBlock* eb = make_block("and.end");

  llvm::IRBuilder<> ir(*get_context());

  // Generate the LHS value and the condition. 
  llvm::Value* lhs = generate_expr(e->get_lhs());
  ir.SetInsertPoint(lb = get_current_block());
  ir.CreateCondBr(lhs, rb, eb);

  // Generate the RHS value and jump.
  emit_block(rb);
  llvm::Value* rhs = generate_expr(e->get_rhs());
  ir.SetInsertPoint(rb = get_current_block());
  ir.CreateBr(eb);

  // Generate the phi node.
  emit_block(eb);
  ir.SetInsertPoint(eb);
  llvm::PHINode* phi = ir.CreatePHI(get_type(e), 2);
  phi->addIncoming(lhs, lb);
  phi->addIncoming(rhs, rb);

  return phi;
}

llvm::Value*
cg_function::generate_or_expr(const binop_expr* e)
{
  llvm::BasicBlock* lb = get_current_block();
  llvm::BasicBlock* rb = make_block("or.else");
  llvm::BasicBlock* eb = make_block("or.end");

  llvm::IRBuilder<> ir(*get_context());

  // Generate the LHS value and the condition. 
  llvm::Value* lhs = generate_expr(e->get_lhs());
  ir.SetInsertPoint(lb = get_current_block());
  ir.CreateCondBr(lhs, eb, rb);

  // Generate the RHS value and jump.
  emit_block(rb);
  llvm::Value* rhs = generate_expr(e->get_rhs());
  ir.SetInsertPoint(rb = get_current_block());
  ir.CreateBr(eb);

  // Generate the phi node.
  emit_block(eb);
  ir.SetInsertPoint(eb);
  llvm::PHINode* phi = ir.CreatePHI(get_type(e), 2);
  phi->addIncoming(lhs, lb);
  phi->addIncoming(rhs, rb);

  return phi;
}

llvm::Value*
cg_function::generate_int_relation(const binop_expr* e)
{
  llvm::Value* lhs = generate_expr(e->get_lhs());
  llvm::Value* rhs = generate_expr(e->get_rhs());
  llvm::IRBuilder<> ir(get_current_block());
  switch (e->get_operator()) {
  case bo_eq:
    return ir.CreateICmpEQ(lhs, rhs);
  case bo_ne:
    return ir.CreateICmpNE(lhs, rhs);
  case bo_lt:
    return ir.CreateICmpSLT(lhs, rhs);
  case bo_gt:
    return ir.CreateICmpSGT(lhs, rhs);
  case bo_le:
    return ir.CreateICmpSLE(lhs, rhs);
  case bo_ge:
    return ir.CreateICmpSGE(lhs, rhs);
  default:
    throw std::logic_error("invalid operator");
  }
}

llvm::Value*
cg_function::generate_fp_relation(const binop_expr* e)
{
  llvm::Value* lhs = generate_expr(e->get_lhs());
  llvm::Value* rhs = generate_expr(e->get_rhs());
  llvm::IRBuilder<> ir(get_current_block());
  switch (e->get_operator()) {
  case bo_eq:
    return ir.CreateFCmpOEQ(lhs, rhs);
  case bo_ne:
    return ir.CreateFCmpONE(lhs, rhs);
  case bo_lt:
    return ir.CreateFCmpOLT(lhs, rhs);
  case bo_gt:
    return ir.CreateFCmpOGT(lhs, rhs);
  case bo_le:
    return ir.CreateFCmpOLE(lhs, rhs);
  case bo_ge:
    return ir.CreateFCmpOGE(lhs, rhs);
  default:
    throw std::logic_error("invalid operator");
  }
}

llvm::Value*
cg_function::generate_relational_expr(const binop_expr* e)
{
  if (e->get_lhs()->is_int())
    return generate_int_relation(e);
  else
    return generate_fp_relation(e);
}

llvm::Value*
cg_function::generate_call_expr(const call_expr* e)
{
  llvm::Value* callee = generate_expr(e->get_callee());

  const expr_list& args = e->get_arguments();
  std::vector<llvm::Value*> vals(args.size());
  std::transform(args.begin(), args.end(), vals.begin(), [this](const expr* e) {
    return generate_expr(e);
  });
  
  llvm::IRBuilder<> ir(get_current_block());
  return ir.CreateCall(callee, vals);
}

llvm::Value*
cg_function::generate_index_expr(const index_expr* e)
{
  throw std::logic_error("not implemented");
}

llvm::Value*
cg_function::generate_assign_expr(const assign_expr* e)
{
  llvm::Value* lhs = generate_expr(e->get_lhs());
  llvm::Value* rhs = generate_expr(e->get_rhs());
  llvm::IRBuilder<> ir(get_current_block());
  ir.CreateStore(rhs, lhs);
  return lhs;
}

llvm::Value*
cg_function::generate_cond_expr(const cond_expr* e)
{
  llvm::BasicBlock* tb = make_block("cond.true");
  llvm::BasicBlock* fb = make_block("cond.false");
  llvm::BasicBlock* eb = make_block("cond.end");

  llvm::IRBuilder<> ir(*get_context());

  // Generate the condition in the current block.
  llvm::Value* cond = generate_expr(e->get_condition());
  ir.SetInsertPoint(get_current_block());
  ir.CreateCondBr(cond, tb, fb);

  // Generate the truth computation.
  emit_block(tb);
  llvm::Value* tval = generate_expr(e->get_true_value());
  ir.SetInsertPoint(tb = get_current_block());
  ir.CreateBr(eb);

  // Generate the truth computation.
  emit_block(fb);
  llvm::Value* fval = generate_expr(e->get_false_value());
  ir.SetInsertPoint(fb = get_current_block());
  ir.CreateBr(eb);

  // Generate the phi node.
  emit_block(eb);
  ir.SetInsertPoint(eb);
  llvm::Type* ty = tval->getType(); // equiv. fval->getType()
  llvm::PHINode* phi = ir.CreatePHI(ty, 2);
  phi->addIncoming(tval, tb);
  phi->addIncoming(fval, fb);
  
  return phi;
}

// FIXME: Clean this up.
llvm::Value*
cg_function::generate_conv_expr(const conv_expr* e)
{
  llvm::Value* val = generate_expr(e->get_source());
  llvm::Type* ty = get_type(e);
  llvm::IRBuilder<> ir(get_current_block());
  switch (e->get_conversion()) {
  case conv_identity:
    return val;
  case conv_value:
    return ir.CreateLoad(val);
  case conv_bool:
    return ir.CreateTrunc(val, ty);
  case conv_char:
    return ir.CreateTrunc(val, ty);
  case conv_int:
    return ir.CreateZExt(val, ty);
  case conv_ext:
    return ir.CreateSIToFP(val, ty);
  case conv_trunc:
    return ir.CreateFPToSI(val, ty);
  }
}

void
cg_function::generate_stmt(const stmt* s)
{
  switch (s->get_kind()) {
  case stmt::block_kind:
    return generate_block_stmt(static_cast<const block_stmt*>(s));
  case stmt::when_kind:
    return generate_when_stmt(static_cast<const when_stmt*>(s));
  case stmt::if_kind:
    return generate_if_stmt(static_cast<const if_stmt*>(s));
  case stmt::while_kind:
    return generate_while_stmt(static_cast<const while_stmt*>(s));
  case stmt::break_kind:
    return generate_break_stmt(static_cast<const break_stmt*>(s));
  case stmt::cont_kind:
    return generate_cont_stmt(static_cast<const cont_stmt*>(s));
  case stmt::ret_kind:
    return generate_ret_stmt(static_cast<const ret_stmt*>(s));
  case stmt::decl_kind:
    return generate_decl_stmt(static_cast<const decl_stmt*>(s));
  case stmt::expr_kind:
    return generate_expr_stmt(static_cast<const expr_stmt*>(s));
  }
}

void
cg_function::generate_block_stmt(const block_stmt* s)
{
  for (const stmt* sub : s->get_statements())
    generate_stmt(sub);
}

void
cg_function::generate_when_stmt(const when_stmt* s)
{
  llvm::BasicBlock* wb = make_block("when.true");
  llvm::BasicBlock* eb = make_block("when.end");

  llvm::IRBuilder<> ir(*get_context());

  // Generate the condition in the current block.
  llvm::Value* cond = generate_expr(s->get_condition());
  ir.SetInsertPoint(get_current_block());
  ir.CreateCondBr(cond, wb, eb);

  // Generate the truth computation.
  emit_block(wb);
  generate_stmt(s->get_body());
  ir.SetInsertPoint(get_current_block());
  ir.CreateBr(eb);

  // Generate the end block
  emit_block(eb);
}

void
cg_function::generate_if_stmt(const if_stmt* s)
{
  llvm::BasicBlock* tb = make_block("if.true");
  llvm::BasicBlock* fb = make_block("if.false");
  llvm::BasicBlock* eb = make_block("if.end");

  llvm::IRBuilder<> ir(*get_context());

  // Generate the condition in the current block.
  llvm::Value* cond = generate_expr(s->get_condition());
  ir.SetInsertPoint(get_current_block());
  ir.CreateCondBr(cond, tb, fb);

  // Generate the true computation.
  emit_block(tb);
  generate_stmt(s->get_true_branch());
  ir.SetInsertPoint(get_current_block());
  ir.CreateBr(eb);

  // Generate the false branch.
  emit_block(fb);
  generate_stmt(s->get_false_branch());
  ir.SetInsertPoint(get_current_block());
  ir.CreateBr(eb);

  // Generate the end block.
  emit_block(eb);
}

void
cg_function::generate_while_stmt(const while_stmt* s)
{
  llvm::BasicBlock* cb = make_block("while.cond");
  llvm::BasicBlock* bb = make_block("while.body");
  llvm::BasicBlock* eb = make_block("while.end");

  llvm::IRBuilder<> ir(*get_context());

  // Emit a branch to the test block.
  ir.CreateBr(cb);

  // Emit the block for the condition.
  emit_block(cb);
  llvm::Value* cond = generate_expr(s->get_condition());
  ir.SetInsertPoint(get_current_block());
  ir.CreateCondBr(cond, bb, eb);

  // Generate the loop body and branch to the condition.
  emit_block(bb);
  generate_stmt(s->get_body());
  ir.SetInsertPoint(get_current_block());
  ir.CreateBr(cb);

  // Generate the end block.
  emit_block(eb);
}

void
cg_function::generate_break_stmt(const break_stmt* e)
{
  throw std::logic_error("not implemented");
}

void
cg_function::generate_cont_stmt(const cont_stmt* e)
{
  throw std::logic_error("not implemented");
}

void
cg_function::generate_ret_stmt(const ret_stmt* s)
{
  // FIXME: Are we initializing the return value as an object or
  // returning directly. Apparently the latter.
  llvm::Value* val = generate_expr(s->get_value());
  llvm::IRBuilder<> ir(get_current_block());
  ir.CreateRet(val);
}

void
cg_function::generate_decl_stmt(const decl_stmt* e)
{
  const decl* d = e->get_declaration();
  switch (d->get_kind()) {
  case decl::var_kind:
    return generate_var_decl(static_cast<const var_decl*>(d));
  case decl::const_kind:
    return generate_const_decl(static_cast<const const_decl*>(d));
  default:
    throw std::logic_error("invalid declaration");
  }
}

void
cg_function::generate_expr_stmt(const expr_stmt* e)
{
  generate_expr(e->get_expression());
}

void
cg_function::generate_var_decl(const var_decl* d)
{
  // Emit an allocation for the variable into the entry block.
  llvm::BasicBlock* entry = get_entry_block();
  llvm::IRBuilder<> ir1(entry, entry->begin());

  llvm::Value* var = ir1.CreateAlloca(get_type(d), nullptr, get_name(d));
  declare(d, var);

  // Generate the initializer and store the result in the current block.
  llvm::Value* init = generate_expr(d->get_init());
  llvm::IRBuilder<> ir2(get_current_block());
  ir2.CreateStore(init, var);
}

void
cg_function::generate_const_decl(const const_decl* d)
{
  // A let declaration binds directly to the value of the initializer.
  // no storage is required for the declaration.
  llvm::Value* obj = generate_expr(d->get_init());
  declare(d, obj);
}

void
generate(const decl* d)
{
  assert(d->is_program());
  
  // Establish the translation context.
  cg_context cg;

  // Create the module, and generate its declarations.
  cg_module mod(cg, static_cast<const prog_decl*>(d));
  mod.generate();

  // Dump the generated module to 
  llvm::outs() << *mod.get_module();
}

