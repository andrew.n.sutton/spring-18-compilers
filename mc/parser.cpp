#include "parser.hpp"

#include <iostream>
#include <sstream>
#include <stdexcept>

/// Returns the name of the token.
inline token_name
parser::lookahead()
{
  assert(!m_tok.empty());
  return m_tok.front().get_name();
}

inline token_name
parser::lookahead(int n)
{
  if (n < m_tok.size())
    return m_tok[n].get_name();
  n = n - m_tok.size() + 1;
  while (n != 0) {
    fetch();
    --n;
  }
  return m_tok.back().get_name();
}

/// If the lookahead matches n, then accept the token and update the 
/// lookahead. Returns the matched token. Otherwise, this is a syntax 
/// error.
token
parser::match(token_name n)
{
  if (lookahead() == n)
    return accept();

  std::stringstream ss;
  ss << peek().get_location() << ": syntax error";
  throw std::runtime_error(ss.str());
}

/// If lookahead matches n, then accept the token and update. Otherwise, 
/// no action is performed, and eof token is returned.
token
parser::match_if(token_name n)
{
  if (lookahead() == n)
    return accept();
  else
    return {};
}

token
parser::match_if_logical_or()
{
  if (lookahead() == tok_logical_operator)
    if (peek().get_logical_operator() == logical_or)
      return accept();
  return {};
}

token
parser::match_if_logical_and()
{
  if (lookahead() == tok_logical_operator)
    if (peek().get_logical_operator() == logical_and)
      return accept();
  return {};
}

token
parser::match_if_bitwise_or()
{
  if (lookahead() == tok_bitwise_operator)
    if (peek().get_bitwise_operator() == op_ior)
      return accept();
  return {};
}

token
parser::match_if_bitwise_xor()
{
  if (lookahead() == tok_bitwise_operator)
    if (peek().get_bitwise_operator() == op_xor)
      return accept();
  return {};
}

token
parser::match_if_bitwise_and()
{
  if (lookahead() == tok_bitwise_operator)
    if (peek().get_bitwise_operator() == op_and)
      return accept();
  return {};
}

/// Matches if the current token is == or !=.
token
parser::match_if_equality()
{
  if (lookahead() == tok_relational_operator) {
    switch (peek().get_relational_operator()) {
    default:
      return {};
    case op_eq:
    case op_ne:
      return accept();
    }
  }
  return {};
}

/// Matches if the current token is <, >, <=, or >=.
token
parser::match_if_relational()
{
  if (lookahead() == tok_relational_operator) {
    switch (peek().get_relational_operator()) {
    default:
      return {};
    case op_lt:
    case op_gt:
    case op_le:
    case op_ge:
      return accept();
    }
  }
  return {};
}

/// Matches if the current token is << or >>.
token
parser::match_if_shift()
{
  if (lookahead() == tok_bitwise_operator) {
    switch (peek().get_bitwise_operator()) {
    default:
      return {};
    case op_shl:
    case op_shr:
      return accept();
    }
  }
  return {};
}

/// Matches if the current token is + or -.
token
parser::match_if_additive()
{
  if (lookahead() == tok_arithmetic_operator) {
    switch (peek().get_arithmetic_operator()) {
    default:
      return {};
    case op_add:
    case op_sub:
      return accept();
    }
  }
  return {};
}

/// Matches if the current token is *, /, %.
token
parser::match_if_multiplicative()
{
  if (lookahead() == tok_arithmetic_operator) {
    switch (peek().get_arithmetic_operator()) {
    default:
      return {};
    case op_mul:
    case op_quo:
    case op_rem:
      return accept();
    }
  }
  return {};
}

/// Matches the '*' in a pointer type.
token
parser::match_if_pointer_glyph()
{
  if (lookahead() == tok_arithmetic_operator)
    if (peek().get_arithmetic_operator() == op_mul)
      return accept();
  return {};
}

/// Matches the '&' in a reference type.
token
parser::match_if_reference_glyph()
{
  if (lookahead() == tok_bitwise_operator)
    if (peek().get_bitwise_operator() == op_and)
      return accept();
  return {};
}

/// Consumes the current token, returning it.
token
parser::accept()
{
  token tok = peek();
  m_tok.pop_front();
  if (m_tok.empty())
    fetch();
  return tok;
}

token
parser::peek()
{
  assert(!m_tok.empty());
  return m_tok.front();
}

void
parser::fetch()
{
  m_tok.push_back(m_lex());
}

// -------------------------------------------------------------------------- //
// Types

/// type -> reference-type
type*
parser::parse_type()
{
  return parse_reference_type();
}

/// basic-type -> void | bool | int | float | char | (type) | (type-list)->type
type*
parser::parse_basic_type()
{
  switch (lookahead()) {
  case tok_type_specifier:
    return m_act.on_basic_type(accept());

  case tok_left_paren: {
    match(tok_left_paren);
    type_list ts;
    if (lookahead() != tok_right_paren)
      ts = parse_type_list();
    match(tok_right_paren);
    if (match_if(tok_arrow_operator)) {
      type* t = parse_type();
      return m_act.on_function_type(ts, t);
    }
    else {
      return m_act.on_nested_type(ts);
    }
  }

  default:
    std::cout << "GOT: " << peek() << '\n';
    throw std::runtime_error("expected basic-type");
  }
}

/// type-list -> type-list ',' type | type
type_list
parser::parse_type_list()
{
  type_list types;
  do {
    type* t = parse_type();
    types.push_back(t);
  } while (match_if(tok_comma));
  return types;
}

/// pointer-type -> pointer-type '*' | basic-type
type*
parser::parse_pointer_type()
{
  type* t = parse_basic_type();
  while (match_if_pointer_glyph())
    t = m_act.on_pointer_type(t);
  return t;
}

/// postfix-type -> pointer-type '&' | pointer-type
type*
parser::parse_reference_type()
{
  type* t = parse_pointer_type();
  if (match_if_reference_glyph())
    return m_act.on_reference_type(t);
  return t;
}


// -------------------------------------------------------------------------- //
// Expressions

expr*
parser::parse_expression()
{
  return parse_assignment_expression();
}

expr*
parser::parse_assignment_expression()
{
  expr* e1 = parse_conditional_expression();
  if (match_if(tok_assignment_operator)) {
    expr* e2 = parse_assignment_expression();
    return m_act.on_assignment_expression(e1, e2);
  }
  return e1;
}

expr*
parser::parse_conditional_expression()
{
  expr* e1 = parse_logical_or_expression();
  if (match_if(tok_conditional_operator)) {
    expr* e2 = parse_expression();
    match(tok_colon);
    expr* e3 = parse_conditional_expression();
    return m_act.on_conditional_expression(e1, e2, e3);
  }
  return e1;
}

expr*
parser::parse_logical_or_expression()
{
  expr* e1 = parse_logical_and_expression();
  while (match_if_logical_or()) {
    expr* e2 = parse_logical_and_expression();
    e1 = m_act.on_logical_or_expression(e1, e2);
  }
  return e1;
}

expr*
parser::parse_logical_and_expression()
{
  expr* e1 = parse_bitwise_or_expression();
  while (match_if_logical_and()) {
    expr* e2 = parse_bitwise_or_expression();
    e1 = m_act.on_logical_and_expression(e1, e2);
  }
  return e1;
}

expr*
parser::parse_bitwise_or_expression()
{
  expr* e1 = parse_bitwise_xor_expression();
  while (match_if_bitwise_or()) {
    expr* e2 = parse_bitwise_xor_expression();
    e1 = m_act.on_bitwise_or_expression(e1, e2);
  }
  return e1;
}

expr*
parser::parse_bitwise_xor_expression()
{
  expr* e1 = parse_bitwise_and_expression();
  while (match_if_bitwise_xor()) {
    expr* e2 = parse_bitwise_and_expression();
    e1 = m_act.on_bitwise_xor_expression(e1, e2);
  }
  return e1;
}

expr*
parser::parse_bitwise_and_expression()
{
  expr* e1 = parse_equality_expression();
  while (match_if_bitwise_and()) {
    expr* e2 = parse_equality_expression();
    e1 = m_act.on_bitwise_and_expression(e1, e2);
  }
  return e1;
}

expr*
parser::parse_equality_expression()
{
  expr* e1 = parse_relational_expression();
  while (token tok = match_if_equality()) {
    expr* e2 = parse_relational_expression();
    e1 = m_act.on_equality_expression(tok, e1, e2);
  }
  return e1;
}

expr*
parser::parse_relational_expression()
{
  expr* e1 = parse_shift_expression();
  while (token tok = match_if_relational()) {
    expr* e2 = parse_shift_expression();
    e1 = m_act.on_relational_expression(tok, e1, e2);
  }
  return e1;
}

expr*
parser::parse_shift_expression()
{
  expr* e1 = parse_additive_expression();
  while (token tok = match_if_shift()) {
    expr* e2 = parse_additive_expression();
    e1 = m_act.on_shift_expression(tok, e1, e2);
  }
  return e1;
}

// additive-expression
//  -> additive-expression + multiplicative-expression
//   | additive-expression - multiplicative-expression
//   | multiplicative-expression
expr*
parser::parse_additive_expression()
{
  expr* e1 = parse_multiplicative_expression();
  while (token tok = match_if_additive()) {
    expr* e2 = parse_multiplicative_expression();
    e1 = m_act.on_additive_expression(tok, e1, e2);
  }
  return e1;
}


expr*
parser::parse_multiplicative_expression()
{
  expr* e1 = parse_cast_expression();
  while (token tok = match_if_multiplicative()) {
    expr* e2 = parse_cast_expression();
    e1 = m_act.on_multiplicative_expression(tok, e1, e2);
  }
  return e1;
}

expr*
parser::parse_cast_expression()
{
  expr* e = parse_unary_expression();
  if (match_if(kw_as)) {
    type* t = parse_type();
    return m_act.on_cast_expression(e, t);
  }
  return e;
}

expr*
parser::parse_unary_expression()
{
  token op;
  switch (lookahead()) {
  case tok_arithmetic_operator:
    switch (peek().get_arithmetic_operator()) {
    case op_add:
    case op_sub:
    case op_mul:
      op = accept();
      break;
    default:
      break;
    }
    break;
  
  case tok_bitwise_operator:
    switch (peek().get_bitwise_operator()) {
    case op_not:
    case op_and:
      op = accept();
      break;
    default:
      break;
    }
    break;
  
  case tok_logical_operator:
    if (peek().get_logical_operator() == logical_not)
      op = accept();
    break;
  
  default:
    break;
  }

  if (op) {
    expr* e = parse_unary_expression();
    return m_act.on_unary_expression(op, e);
  }

  return parse_postfix_expression();
}

expr*
parser::parse_postfix_expression()
{
  expr* e = parse_primary_expression();
  while (true) {
    if (match_if(tok_left_paren)) {
      expr_list args;
      if (lookahead() != tok_right_paren)
        args = parse_argument_list();
      match(tok_right_paren);
      e = m_act.on_call_expression(e, args); 
    }
    else if (match_if(tok_left_bracket)) {
      expr_list args;
      if (lookahead() != tok_right_bracket)
        args = parse_argument_list();
      match(tok_right_bracket);
      e = m_act.on_index_expression(e, args); 
    }
    else {
      break;
    }
  }
  return e;
}

expr*
parser::parse_primary_expression()
{
  switch (lookahead()) {
  case tok_binary_integer:
  case tok_decimal_integer:
  case tok_hexadecimal_integer:
    return m_act.on_integer_literal(accept());
  
  case tok_boolean:
    return m_act.on_boolean_literal(accept());
  
  case tok_floating_point:
    return m_act.on_float_literal(accept());
  
  case tok_character:
  case tok_string:
    throw std::logic_error("not implemented");
  
  case tok_identifier:
    return m_act.on_id_expression(accept());
  
  case tok_left_paren: {
    match(tok_left_paren);
    expr* e = parse_expression();
    match(tok_right_paren);
    return e;
  }

  default:
    break;
  }
  
  throw std::runtime_error("expected primary-expression");
}

expr_list
parser::parse_argument_list()
{
  expr_list args;
  do {
    expr* arg = parse_expression();
    args.push_back(arg);
  } while (match_if(tok_comma));
  return args;
}

// ---------------------------------------------------------------------------//
// Statements

stmt*
parser::parse_statement()
{
  switch (lookahead()) {
  case kw_if:
    return parse_if_statement();
  case kw_while:
    return parse_while_statement();
  case kw_break:
    return parse_break_statement();
  case kw_continue:
    return parse_continue_statement();
  case kw_return:
    return parse_return_statement();
  case kw_var:
  case kw_let:
  case kw_def:
    return parse_declaration_statement();
  case tok_left_brace:
    return parse_block_statement();
  default:
    return parse_expression_statement();
  }
}

stmt*
parser::parse_block_statement()
{
  match(tok_left_brace);
  m_act.enter_block_scope();
  m_act.start_block();
  
  stmt_list ss;
  if (lookahead() != tok_right_brace)
    ss = parse_statement_seq();
  
  m_act.finish_block();
  m_act.leave_scope();
  match(tok_right_brace);
  return m_act.on_block_statement(ss);
}

stmt*
parser::parse_if_statement()
{
  assert(lookahead() == kw_if);
  accept();
  match(tok_left_paren);
  expr* e = parse_expression();
  match(tok_right_paren);
  stmt* t = parse_statement();
  match(kw_else);
  stmt* f = parse_statement();
  return m_act.on_if_statement(e, t, f);
}

stmt*
parser::parse_while_statement()
{
  assert(lookahead() == kw_while);
  accept();
  match(tok_left_paren);
  expr* e = parse_expression();
  match(tok_right_paren);
  stmt* b = parse_statement();
  return m_act.on_while_statement(e, b);
}

stmt*
parser::parse_break_statement()
{
  assert(lookahead() == kw_break);
  accept();
  match(tok_semicolon);
  return m_act.on_break_statement();
}

stmt*
parser::parse_continue_statement()
{
  assert(lookahead() == kw_continue);
  accept();
  match(tok_semicolon);
  return m_act.on_continue_statement();
}

stmt*
parser::parse_return_statement()
{
  assert(lookahead() == kw_return);
  accept();
  expr* e = parse_expression();
  match(tok_semicolon);
  return m_act.on_return_statement(e);
}

stmt*
parser::parse_declaration_statement()
{
  decl* d = parse_local_declaration();
  return m_act.on_declaration_statement(d);
}

stmt*
parser::parse_expression_statement()
{
  expr* e = parse_expression();
  match(tok_semicolon);
  return m_act.on_expression_statement(e);
}

stmt_list 
parser::parse_statement_seq()
{
  stmt_list ss;
  while (true) {
    stmt* s = parse_statement();
    ss.push_back(s);
    if (lookahead() == tok_right_brace)
      break;
  }
  return ss;
}


// ---------------------------------------------------------------------------//
// Declarations

decl* 
parser::parse_declaration()
{
  switch (lookahead()) {
  default:
    throw std::runtime_error("expected declaration");
  case kw_def: {
    token_name n = lookahead(2);
    if (n == tok_colon)
      return parse_object_definition();
    if (n == tok_left_paren)
      return parse_function_definition();
    throw std::runtime_error("wrong");
  }
  case kw_let:
  case kw_var:
    return parse_object_definition();
  }
  return nullptr;
}

decl*
parser::parse_local_declaration()
{
  return parse_object_definition();
} 

decl*
parser::parse_object_definition() 
{
  switch (lookahead()) {
  default:
    throw std::runtime_error("expected object-definition");  
  case kw_def:
    return parse_value_definition();
  case kw_let:
    return parse_constant_definition();
  case kw_var:
    return parse_variable_definition();
  }
}

decl*
parser::parse_variable_definition()
{
  assert(lookahead() == kw_var);
  accept();
  token id = match(tok_identifier);
  match(tok_colon);
  type* t = parse_type();

  // Point of declaration
  decl* d = m_act.on_variable_declaration(id, t);

  match(tok_assignment_operator);
  expr* e = parse_expression();
  match(tok_semicolon);

  // End of definition
  return m_act.on_variable_definition(d, e);
}

decl*
parser::parse_constant_definition()
{
  assert(lookahead() == kw_let);
  accept();
  token id = match(tok_identifier);
  match(tok_colon);
  type* t = parse_type();

  // Point of declaration
  decl* d = m_act.on_constant_declaration(id, t);

  match(tok_assignment_operator);
  expr* e = parse_expression();
  match(tok_semicolon);

  // End of definition
  return m_act.on_constant_definition(d, e);
}

decl*
parser::parse_value_definition()
{
  assert(lookahead() == kw_def);
  accept();
  token id = match(tok_identifier);
  match(tok_colon);
  type* t = parse_type();

  // Point of declaration.
  decl* d = m_act.on_value_declaration(id, t);

  match(tok_assignment_operator);
  expr* e = parse_expression();
  match(tok_semicolon);

  // End of definition.
  return m_act.on_value_definition(d, e);
}

decl*
parser::parse_function_definition()
{
  assert(lookahead() == kw_def);
  accept();
  token id = match(tok_identifier);

  match(tok_left_paren);
  m_act.enter_parameter_scope();
  decl_list parms;
  if (lookahead() != tok_right_paren)
    parms = parse_parameter_clause();
  m_act.leave_scope();
  match(tok_right_paren);
  
  match(tok_arrow_operator);
  
  type* t = parse_type();

  // Point of declaration
  decl* d = m_act.on_function_declaration(id, parms, t);

  // FIXME: We want a semantic inside the braces so that we can declare
  // parameters. Maybe parse this differently than block statement.
  stmt* s = parse_block_statement();
  
  // End of definition
  return m_act.on_function_definition(d, s);
}

/// parameter-clause -> parameter-list
///                   | parameter-list ',' ...
///
/// \todo Support variadic parameters.
decl_list
parser::parse_parameter_clause()
{
  return parse_parameter_list();
}


// parameter-list -> parameter | parameter-list ',' parameter
decl_list
parser::parse_parameter_list()
{
  decl_list parms;
  while (true) {
    // FIXME: If lookahead is ..., this is the variadic parameter.
    parms.push_back(parse_parameter());
    if (match_if(tok_comma))
      continue;
    else
      break;
  }
  return parms;
}

decl*
parser::parse_parameter()
{
  token id = match(tok_identifier);
  match(tok_colon);
  type* t = parse_type();
  return m_act.on_parameter_declaration(id, t);
}

decl_list
parser::parse_declaration_seq()
{
  decl_list ds;
  while (peek()) {
    decl* d = parse_declaration();
    ds.push_back(d);
  }
  return ds;
}

decl*
parser::parse_program()
{
  m_act.enter_global_scope();
  decl_list decls = parse_declaration_seq();
  m_act.leave_scope();
  return m_act.on_program(decls);
}
