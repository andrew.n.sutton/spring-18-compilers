#include "debug.hpp"
#include "type.hpp"
#include "expr.hpp"
#include "stmt.hpp"
#include "decl.hpp"

#include <iostream>

struct node_font
{
  node_font(const char* s)
    : name(s)
  { }
  
  const char* name;
};

std::ostream& 
operator<<(std::ostream& os, node_font f)
{
  return os << "\033[1;30m" << f.name << "\033[0m";
}

struct address_font
{
  address_font(const void* p)
    : ptr(p)
  { }
  
  const void* ptr;
};

std::ostream& 
operator<<(std::ostream& os, address_font f)
{
  return os << "\033[32m" << f.ptr << "\033[0m";
}

struct node_header
{
  node_header(debug_printer& dp, const char* name, const void* addr)
    : m_dp(dp), m_body(false)
  {
    std::string tab(dp.nesting() * 2, ' ');
    m_dp.get_stream() << tab << node_font(name) << ' ' << address_font(addr);
  }

  ~node_header()
  {
    if (!m_body)
      m_dp.get_stream() << '\n';
  }

  debug_printer& m_dp;
  bool m_body;
};

struct node_body
{
  node_body(node_header& h)
    : m_dp(h.m_dp)
  {
    h.m_body = true;
    m_dp.get_stream() << '\n';
    m_dp.indent();
  }

  ~node_body()
  {
    m_dp.undent();
  }

  debug_printer& m_dp;
};

static const char*
get_node_name(const type* t) 
{
  switch (t->get_kind()) {
  case type::bool_kind: return "bool-type";
  case type::char_kind: return "char-type";
  case type::int_kind: return "int-type";
  case type::float_kind: return "float-type";
  case type::ptr_kind: return "ptr-type";
  case type::ref_kind: return "ref-type";
  case type::fn_kind: return "fn-type";
  }
}

// FIXME: Recurse?
void
debug(debug_printer& dp, const type* t)
{
  node_header n(dp, get_node_name(t), t);
}

static const char*
get_node_name(const expr* e)
{
  switch (e->get_kind()) {
  case expr::bool_kind: return "bool-expr";
  case expr::int_kind: return "int-expr";
  case expr::float_kind: return "float-expr";
  case expr::id_kind: return "id-expr";
  case expr::unop_kind: return "unop-expr";
  case expr::binop_kind: return "binop-expr";
  case expr::call_kind: return "call-expr";
  case expr::index_kind: return "index-expr";
  case expr::cast_kind: return "cast-expr";
  case expr::assign_kind: return "assign-expr";
  case expr::cond_kind: return "cond-expr";
  case expr::conv_kind: return "conv-expr";
  }
}

static void
debug_attrs(debug_printer& dp, const bool_expr* e)
{
  dp.get_stream() << " value=" << e->get_value();
}

static void
debug_attrs(debug_printer& dp, const int_expr* e)
{
  dp.get_stream() << " value=" << e->get_value();
}

static void
debug_attrs(debug_printer& dp, const float_expr* e)
{
  dp.get_stream() << " value=" << e->get_value();
}

static void
debug_attrs(debug_printer& dp, const id_expr* e)
{
  decl* d = e->get_declaration();
  dp.get_stream() << " name=" << *d->get_name();
  dp.get_stream() << " ref=" << d;
}

static const char*
get_operator_name(unop op)
{
  switch (op) {
  case uo_pos: return "+";
  case uo_neg: return "-";
  case uo_cmp: return "~";
  case uo_not: return "not";
  case uo_addr: return "&";
  case uo_deref: return "*";
  }
}

static const char*
get_operator_name(const unop_expr* e)
{
  return get_operator_name(e->get_operator());
}

static void
debug_attrs(debug_printer& dp, const unop_expr* e)
{
  dp.get_stream() << "  op=" << get_operator_name(e);
}

static const char*
get_operator_name(binop op)
{
  switch (op) {
  case bo_add: return "+";
  case bo_sub: return "-";
  case bo_mul: return "*";
  case bo_quo: return "/";
  case bo_rem: return "%";
  case bo_and: return "&";
  case bo_ior: return "|";
  case bo_xor: return "^";
  case bo_shl: return "<<";
  case bo_shr: return ">>";
  case bo_land: return "and";
  case bo_lor: return "or";
  case bo_eq: return "==";
  case bo_ne: return "!=";
  case bo_lt: return "<";
  case bo_gt: return ">";
  case bo_le: return "<=";
  case bo_ge: return "<=";
  }
}

static const char*
get_operator_name(const binop_expr* e)
{
  return get_operator_name(e->get_operator());
}

static void
debug_attrs(debug_printer& dp, const binop_expr* e)
{
  dp.get_stream() << "  op=" << get_operator_name(e);
}

static void
debug_attrs(debug_printer& dp, const call_expr* e)
{
}

static void
debug_attrs(debug_printer& dp, const index_expr* e)
{
}

static void
debug_attrs(debug_printer& dp, const cast_expr* e)
{
}

static void
debug_attrs(debug_printer& dp, const assign_expr* e)
{
}

static void
debug_attrs(debug_printer& dp, const cond_expr* e)
{
}

static const char*
get_conversion_name(conversion c)
{
  switch (c) {
  case conv_identity: return "id";
  case conv_value: return "value";
  case conv_bool: return "bool";
  case conv_char: return "char";
  case conv_int: return "int";
  case conv_ext: return "ext";
  case conv_trunc: return "trunc";
  }
}

static const char*
get_conversion_name(const conv_expr* e)
{
  return get_conversion_name(e->get_conversion());
}

static void
debug_attrs(debug_printer& dp, const conv_expr* e)
{
  dp.get_stream() << ' ' << get_conversion_name(e);
}

static void
debug_attrs(debug_printer& dp, const expr* e)
{
  switch (e->get_kind()) {
  case expr::bool_kind:
    return debug_attrs(dp, static_cast<const bool_expr*>(e));
  case expr::int_kind:
    return debug_attrs(dp, static_cast<const int_expr*>(e));
  case expr::float_kind:
    return debug_attrs(dp, static_cast<const float_expr*>(e));
  case expr::id_kind:
    return debug_attrs(dp, static_cast<const id_expr*>(e));
  case expr::unop_kind:
    return debug_attrs(dp, static_cast<const unop_expr*>(e));
  case expr::binop_kind:
    return debug_attrs(dp, static_cast<const binop_expr*>(e));
  case expr::call_kind:
    return debug_attrs(dp, static_cast<const call_expr*>(e));
  case expr::index_kind:
    return debug_attrs(dp, static_cast<const index_expr*>(e));
  case expr::cast_kind:
    return debug_attrs(dp, static_cast<const cast_expr*>(e));
  case expr::assign_kind:
    return debug_attrs(dp, static_cast<const assign_expr*>(e));
  case expr::cond_kind:
    return debug_attrs(dp, static_cast<const cond_expr*>(e));
  case expr::conv_kind:
    return debug_attrs(dp, static_cast<const conv_expr*>(e));
  }
}

static void
debug_subexprs(debug_printer& dp, const unop_expr* e)
{
  debug(dp, e->get_operand());
}

static void
debug_subexprs(debug_printer& dp, const binop_expr* e)
{
  debug(dp, e->get_lhs());
  debug(dp, e->get_rhs());
}

static void
debug_subexprs(debug_printer& dp, const call_expr* e)
{

}

static void
debug_subexprs(debug_printer& dp, const index_expr* e)
{

}

static void
debug_subexprs(debug_printer& dp, const cast_expr* e)
{

}

static void
debug_subexprs(debug_printer& dp, const assign_expr* e)
{
  debug(dp, e->get_lhs());
  debug(dp, e->get_rhs());
}

static void
debug_subexprs(debug_printer& dp, const cond_expr* e)
{
  debug(dp, e->get_condition());
  debug(dp, e->get_true_value());
  debug(dp, e->get_false_value());
}

static void
debug_subexprs(debug_printer& dp, const conv_expr* e)
{
  debug(dp, e->get_source());
}

static void
debug_subexprs(debug_printer& dp, const expr* e)
{
  switch (e->get_kind()) {
  case expr::bool_kind:
  case expr::int_kind:
  case expr::float_kind:
  case expr::id_kind:
    return;
  case expr::unop_kind:
    return debug_subexprs(dp, static_cast<const unop_expr*>(e));
  case expr::binop_kind:
    return debug_subexprs(dp, static_cast<const binop_expr*>(e));
  case expr::call_kind:
    return debug_subexprs(dp, static_cast<const call_expr*>(e));
  case expr::index_kind:
    return debug_subexprs(dp, static_cast<const index_expr*>(e));
  case expr::cast_kind:
    return debug_subexprs(dp, static_cast<const cast_expr*>(e));
  case expr::assign_kind:
    return debug_subexprs(dp, static_cast<const assign_expr*>(e));
  case expr::cond_kind:
    return debug_subexprs(dp, static_cast<const cond_expr*>(e));
  case expr::conv_kind:
    return debug_subexprs(dp, static_cast<const conv_expr*>(e));
  }
}

void
debug(debug_printer& dp, const expr* e)
{
  node_header head(dp, get_node_name(e), e);  
  debug_attrs(dp, e);

  node_body body(head);
  debug(dp, e->get_type());  
  debug_subexprs(dp, e);
}

static const char*
get_node_name(const stmt* s)
{
  switch (s->get_kind()) {
  case stmt::block_kind: return "block-stmt";
  case stmt::when_kind: return "when-stmt";
  case stmt::if_kind: return "if-stmt";
  case stmt::while_kind: return "while-stmt";
  case stmt::break_kind: return "break-stmt";
  case stmt::cont_kind: return "cont-stmt";
  case stmt::ret_kind: return "ret-stmt";
  case stmt::decl_kind: return "decl-stmt";
  case stmt::expr_kind: return "expr-stmt";
  }
}

static void
debug_children(debug_printer& dp, const stmt_list& stmts)
{
  dp.indent();
  for (const stmt* s : stmts)
    debug(dp, s);
  dp.undent();
}

void
debug_stmt(debug_printer& dp, const block_stmt* s)
{
  debug_children(dp, s->get_statements());
}

void
debug_stmt(debug_printer& dp, const when_stmt* s)
{
  dp.indent();
  debug(dp, s->get_condition());
  debug(dp, s->get_body());
  dp.undent();
}

void
debug_stmt(debug_printer& dp, const if_stmt* s)
{
  dp.indent();
  debug(dp, s->get_condition());
  debug(dp, s->get_true_branch());
  debug(dp, s->get_false_branch());
  dp.undent();
}

void
debug_stmt(debug_printer& dp, const while_stmt* s)
{
  dp.indent();
  debug(dp, s->get_condition());
  debug(dp, s->get_body());
  dp.undent();
}

void
debug_stmt(debug_printer& dp, const ret_stmt* s)
{
}

void
debug_stmt(debug_printer& dp, const decl_stmt* s)
{
}

void
debug_stmt(debug_printer& dp, const expr_stmt* s)
{
}

void
debug(debug_printer& dp, const stmt* s)
{
  node_header n(dp, get_node_name(s), s);
  switch (s->get_kind()) {
  case stmt::block_kind:
    return debug_stmt(dp, static_cast<const block_stmt*>(s));
  case stmt::when_kind:
    return debug_stmt(dp, static_cast<const when_stmt*>(s));
  case stmt::if_kind:
    return debug_stmt(dp, static_cast<const if_stmt*>(s));
  case stmt::while_kind:
    return debug_stmt(dp, static_cast<const while_stmt*>(s));
  case stmt::break_kind:
  case stmt::cont_kind:
    return;
  case stmt::ret_kind:
    return debug_stmt(dp, static_cast<const ret_stmt*>(s));
  case stmt::decl_kind:
    return debug_stmt(dp, static_cast<const decl_stmt*>(s));
  case stmt::expr_kind:
    return debug_stmt(dp, static_cast<const expr_stmt*>(s));
  }
}

static const char*
get_node_name(const decl* d)
{
  switch (d->get_kind()) {
  case decl::prog_kind: return "prog-decl";
  case decl::var_kind: return "var-decl";
  case decl::const_kind: return "const-decl";
  case decl::value_kind: return "value-decl";
  case decl::parm_kind: return "parm-decl";
  case decl::fn_kind: return "fn-decl";
  }
}

static void
start_node(debug_printer& dp, const char* node, const decl* d)
{
  std::string tab(dp.nesting() * 2, ' ');
  dp.get_stream() << tab << node_font(node) << ' ' << address_font(d);
  if (d->get_name())
    dp.get_stream() << ' ' << "name=" << *d->get_name();
  dp.get_stream() << '\n';
}

static void
debug_children(debug_printer& dp, const decl_list& decls)
{
  for (const decl* d : decls)
    debug(dp, d);
}

static void
debug_decl(debug_printer& dp, const prog_decl* d)
{
  debug_children(dp, d->get_declarations());
}

static void
debug_decl(debug_printer& dp, const object_decl* d)
{
  debug(dp, d->get_type());
  if (const expr* e = d->get_init())
    debug(dp, e);
}

static void
debug_decl(debug_printer& dp, const fn_decl* d)
{
  debug_children(dp, d->get_parameters());
  debug(dp, d->get_return_type());
  if (d->get_body())
    debug(dp, d->get_body());
}

void
debug(debug_printer& dp, const decl* d)
{
  node_header head(dp, get_node_name(d), d);
  
  node_body body(head);
  switch (d->get_kind()) {
  case decl::prog_kind:
    return debug_decl(dp, static_cast<const prog_decl*>(d));
  case decl::var_kind:
    return debug_decl(dp, static_cast<const var_decl*>(d));
  case decl::const_kind:
    return debug_decl(dp, static_cast<const const_decl*>(d));
  case decl::value_kind:
    return debug_decl(dp, static_cast<const value_decl*>(d));
  case decl::parm_kind:
    return debug_decl(dp, static_cast<const parm_decl*>(d));
  case decl::fn_kind:
    return debug_decl(dp, static_cast<const fn_decl*>(d));
  }
}
