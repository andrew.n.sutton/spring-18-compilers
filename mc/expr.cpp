#include "expr.hpp"
#include "type.hpp"
#include "debug.hpp"

#include <iostream>

type*
expr::get_object_type() const
{
  return m_type->get_object_type();
}

bool
expr::has_type(const type* t) const
{
  return is_same_as(m_type, t);
}

bool
expr::is_bool() const
{
  return m_type->is_bool();
}

bool
expr::is_int() const
{
  return m_type->is_int();
}

bool
expr::is_float() const
{
  return m_type->is_float();
}

bool
expr::is_function() const
{
  return m_type->is_function();
}

bool
expr::is_arithmetic() const
{
  return m_type->is_arithmetic();
}

bool
expr::is_numeric() const
{
  return m_type->is_numeric();
}

bool
expr::is_scalar() const
{
  return m_type->is_scalar();
}

void
expr::debug() const
{
  debug_printer dp(std::cerr);
  ::debug(dp, this);
}
