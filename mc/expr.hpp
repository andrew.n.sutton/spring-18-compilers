#pragma once

#include "token.hpp"

#include <vector>

class type;
class decl;

/// Represents the set of all expressions.
class expr
{
public:
  enum kind {
    bool_kind,
    int_kind,
    float_kind,
    id_kind,
    unop_kind,
    binop_kind,
    call_kind,
    index_kind,
    cast_kind,
    assign_kind,
    cond_kind,
    conv_kind,
  };

protected:
  expr(kind k)
    : m_kind(k), m_type()
  { }

  expr(kind k, type* t)
    : m_kind(k), m_type(t)
  { }

public:
  virtual ~expr() = default;

  /// Returns the kind of expression.
  kind get_kind() const { return m_kind; }

  /// Returns the type of the expression.
  type* get_type() const { return m_type; }

  /// Returns the object type of the expression.
  type* get_object_type() const;

  /// Returns true if this expression has type t.
  bool has_type(const type* t) const;

  bool is_bool() const;
  bool is_int() const;
  bool is_float() const;
  bool is_function() const;
  bool is_arithmetic() const;
  bool is_numeric() const;
  bool is_scalar() const;

  void debug() const;

private:
  kind m_kind;
  type* m_type;
};


using expr_list = std::vector<expr*>;


// Boolean literals
struct bool_expr : expr
{
  bool_expr(type* t, bool b)
    : expr(bool_kind, t), val(b)
  { }

  bool get_value() const { return val; }

  bool val;
};


// Integer literals.
struct int_expr : expr
{
  int_expr(type* t, int n)
    : expr(int_kind, t), val(n)
  { }

  int get_value() const { return val; }

  int val;
};


// Floating point literals.
struct float_expr : expr
{
  float_expr(type* t, double n)
    : expr(float_kind, t), val(n)
  { }

  double get_value() const { return val; }

  double val;
};


/// An expression that denotes an entity.
struct id_expr : expr
{
  id_expr(type* t, decl* d)
    : expr(id_kind, t), ref(d)
  { }

  decl* get_declaration() const { return ref; }
  
  decl* ref;
};


enum unop
{
  uo_pos,
  uo_neg,
  uo_cmp,
  uo_not,
  uo_addr,
  uo_deref,
};


/// Represents unary expressions of the form @e1.
struct unop_expr : expr
{
  unop_expr(type* t, unop op, expr* e1)
    : expr(unop_kind, t), m_op(op), m_arg(e1)
  { }

  unop get_operator() const { return m_op; }
  expr* get_operand() const { return m_arg; }

  unop m_op;
  expr* m_arg;
};


/// Kinds of binary operator
enum binop 
{
  bo_add,
  bo_sub,
  bo_mul,
  bo_quo,
  bo_rem,
  bo_and,
  bo_ior,
  bo_xor,
  bo_shl,
  bo_shr,
  bo_land,
  bo_lor,
  bo_eq,
  bo_ne,
  bo_lt,
  bo_gt,
  bo_le,
  bo_ge,
};

/// Represents binary expressions of the form e1 @ e2.
struct binop_expr : expr
{
  binop_expr(type* t, binop op, expr* e1, expr* e2)
    : expr(binop_kind, t), m_op(op), m_lhs(e1), m_rhs(e2)
  { }

  binop get_operator() const { return m_op; }

  expr* get_lhs() const { return m_lhs; }
  expr* get_rhs() const { return m_rhs; }

  binop m_op;
  expr* m_lhs;
  expr* m_rhs;
};


/// Base class of call and index expressions.
struct postfix_expr : expr
{
  postfix_expr(kind k, type* t, expr* e, const expr_list& args)
    : expr(k, t), m_base(e), m_args(args)
  { }

  const expr_list& get_arguments() const { return m_args; }
  expr_list& get_arguments() { return m_args; }
  
  expr* m_base;
  expr_list m_args;
};

/// Represents call expressions e.g., f(args)
struct call_expr : postfix_expr
{
  call_expr(type* t, expr* e, const expr_list& args)
    : postfix_expr(call_kind, t, e, args)
  { }

  expr* get_callee() const { return m_base; }
};


/// Represents index expressions e.g. a[args].
struct index_expr : postfix_expr
{
  index_expr(type* t, expr* e, const expr_list& args)
    : postfix_expr(index_kind, t, e, args)
  { }
};


/// Represents cast expressions of the for e1 as t1.
struct cast_expr : expr
{
  cast_expr(expr* e, type* t)
    : expr(cast_kind, t), m_src(e), m_dst(t)
  { }

  expr* m_src;
  type* m_dst;
};


/// Represents assignment expressions of the form e1 = e2.
struct assign_expr : expr
{
  assign_expr(type* t, expr* e1, expr* e2)
    : expr(assign_kind), m_lhs(e1), m_rhs(e2)
  { }

  expr* get_lhs() const { return m_lhs; }
  expr* get_rhs() const { return m_rhs; }

  expr* m_lhs;
  expr* m_rhs;
};


/// Represents conditional expressions of the form e1 ? e2 ? e3.
struct cond_expr : expr
{
  cond_expr(type* t, expr* e1, expr* e2, expr* e3)
    : expr(cond_kind, t), m_cond(e1), m_true(e2), m_false(e3)
  { }

  expr* get_condition() const { return m_cond; }
  expr* get_true_value() const { return m_true; }
  expr* get_false_value() const { return m_false; }

  expr* m_cond;
  expr* m_true;
  expr* m_false;
};

enum conversion 
{
  conv_identity, // No conversion
  conv_value, // Conversion to value
  conv_bool, // Conversion from T to bool
  conv_char, // Conversion from int to char
  conv_int, // Conversion from T to int
  conv_ext, // Conversion for int to float
  conv_trunc, // Conversion from float to int
};

struct conv_expr : expr
{
  conv_expr(expr* e1, conversion c, type* t)
    : expr(conv_kind, t), m_src(e1), m_conv(c)
  { }

  conversion get_conversion() const { return m_conv; }

  expr* get_source() const { return m_src; }
  
  expr* m_src;
  conversion m_conv;
};
