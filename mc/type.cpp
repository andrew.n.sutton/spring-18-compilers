#include "type.hpp"
#include "debug.hpp"

#include <iostream>

bool
type::is_reference_to(const type* t)
{
  if (const ref_type* rt = dynamic_cast<const ref_type*>(this))
    return is_same_as(rt->get_object_type(), t);
  return false;
}

bool
type::is_pointer_to(const type* t)
{
  if (const ptr_type* rt = dynamic_cast<const ptr_type*>(this))
    return is_same_as(rt->get_element_type(), t);
  return false;
}

bool
type::is_arithmetic() const
{
  return m_kind == int_kind || m_kind == float_kind; 
}

bool
type::is_numeric() const
{
  switch (m_kind) {
  case bool_kind:
  case char_kind:
  case int_kind:
  case float_kind:
    return true;
  default:
    return false;
  }
}

type*
type::get_object_type() const
{
  if (const ref_type* rt = dynamic_cast<const ref_type*>(this))
    return rt->get_object_type();
  return const_cast<type*>(this);
}

void
type::debug() const
{
  debug_printer dp(std::cerr);
  ::debug(dp, this);
}

static bool
is_same_as_ptr(const ptr_type* t1, const ptr_type* t2)
{
  return is_same_as(t1->get_element_type(), 
                    t2->get_element_type());
}

static bool
is_same_as_ref(const ref_type* t1, const ref_type* t2)
{
  return is_same_as(t1->get_object_type(), t2->get_object_type());
}

static bool
is_same_as_fn(const fn_type* t1, const fn_type* t2)
{
  auto cmp = [](const type* a, const type* b) {
    return is_same_as(a, b);
  };
  const type_list& p1 = t1->get_parameter_types();
  const type_list& p2 = t2->get_parameter_types();
  return std::equal(p1.begin(), p1.end(), p2.begin(), p2.end(), cmp);
}


bool
is_same_as(const type* t1, const type* t2)
{
  if (t1 == t2)
    return true;

  if (t1->get_kind() != t2->get_kind())
    return false;

  switch (t1->get_kind()) {
  case type::bool_kind:
  case type::char_kind:
  case type::int_kind:
  case type::float_kind:
    return true;
  case type::ptr_kind:
    return is_same_as_ptr(static_cast<const ptr_type*>(t1),
                          static_cast<const ptr_type*>(t2));
  case type::ref_kind:
    return is_same_as_ref(static_cast<const ref_type*>(t1),
                          static_cast<const ref_type*>(t2));
  case type::fn_kind:
    return is_same_as_fn(static_cast<const fn_type*>(t1),
                         static_cast<const fn_type*>(t2));
  }
}