#include "semantics.hpp"
#include "type.hpp"
#include "expr.hpp"
#include "stmt.hpp"
#include "decl.hpp"
#include "scope.hpp"

#include <iostream>
#include <sstream>

semantics::semantics()
  : m_scope(nullptr), 
    m_fn(nullptr), 
    m_bool(new bool_type()),
    m_char(new char_type()),
    m_int(new int_type()),
    m_float(new float_type())
{ }

semantics::~semantics()
{
  assert(!m_scope); // The scope must be empty.
  assert(!m_fn);
}

type* 
semantics::on_basic_type(token tok)
{
  switch (tok.get_type_specifier()) {
  case ts_bool:
    return m_bool;
  case ts_char:
    return m_char;
  case ts_int:
    return m_int;
  case ts_float:
    return m_float;
  }
}

type*
semantics::on_nested_type(const type_list& ts)
{
  if (ts.size() == 0)
    throw std::runtime_error("empty type-list");
  if (ts.size() > 1)
    throw std::runtime_error("multiple types in type-list");
  return ts.front();
}

type*
semantics::on_function_type(const type_list& ts, type* t)
{
  return new fn_type(ts, t);
}

type*
semantics::on_pointer_type(type* t)
{
  return new ptr_type(t);
}

type*
semantics::on_reference_type(type* t)
{
  return new ref_type(t);
}

expr* 
semantics::on_assignment_expression(expr* e1, expr* e2)
{
  e1 = require_reference(e1);
  e2 = require_value(e2);
  
  type* t1 = e1->get_object_type();
  type* t2 = e2->get_type();
  require_same(t1, t2);
  
  return new assign_expr(e1->get_type(), e1, e2);
}

expr* 
semantics::on_conditional_expression(expr* e1, expr* e2, expr* e3)
{
  e1 = require_boolean(e1);

  type* c = common_type(e2->get_type(), e3->get_type());
  e2 = convert_to_type(e2, c);
  e3 = convert_to_type(e3, c);

  return new cond_expr(c, e1, e2, e3);
}

expr* 
semantics::on_logical_or_expression(expr* e1, expr* e2)
{
  e1 = require_boolean(e1);
  e2 = require_boolean(e2);
  return new binop_expr(m_bool, bo_lor, e1, e2);
}

expr* 
semantics::on_logical_and_expression(expr* e1, expr* e2)
{
  e1 = require_boolean(e1);
  e2 = require_boolean(e2);
  return new binop_expr(m_bool, bo_land, e1, e2);
}

expr* 
semantics::on_bitwise_or_expression(expr* e1, expr* e2)
{
  e1 = require_integer(e1);
  e2 = require_integer(e2);
  return new binop_expr(m_int, bo_ior, e1, e2);
}

expr* 
semantics::on_bitwise_xor_expression(expr* e1, expr* e2)
{
  e1 = require_integer(e1);
  e2 = require_integer(e2);
  return new binop_expr(m_int, bo_xor, e1, e2);
}

expr* 
semantics::on_bitwise_and_expression(expr* e1, expr* e2)
{
  e1 = require_integer(e1);
  e2 = require_integer(e2);
  return new binop_expr(m_int, bo_and, e1, e2);
}

static binop
get_relational_op(relational_op op)
{
  switch (op) {
  case op_eq: return bo_eq;
  case op_ne: return bo_ne;
  case op_lt: return bo_lt;
  case op_gt: return bo_gt;
  case op_le: return bo_le;
  case op_ge: return bo_ge;
  }
}

expr* 
semantics::on_equality_expression(token tok, expr* e1, expr* e2)
{
  e1 = require_scalar(e1);
  e2 = require_scalar(e2);
  relational_op op = tok.get_relational_operator();
  return new binop_expr(m_bool, get_relational_op(op), e1, e2); 
}

expr* 
semantics::on_relational_expression(token tok, expr* e1, expr* e2)
{
  e1 = require_numeric(e1);
  e2 = require_numeric(e2);
  relational_op op = tok.get_relational_operator();
  return new binop_expr(m_bool, get_relational_op(op), e1, e2); 
}

static binop
get_bitwise_op(bitwise_op op)
{
  switch (op) {
  case op_and: return bo_and;
  case op_ior: return bo_ior;
  case op_xor: return bo_xor;
  case op_shl: return bo_shl;
  case op_shr: return bo_shr;
  default:
    throw std::logic_error("invalid operator");
  }
}

expr* 
semantics::on_shift_expression(token tok, expr* e1, expr* e2)
{
  e1 = require_integer(e1);
  e2 = require_integer(e2);
  bitwise_op op = tok.get_bitwise_operator();
  return new binop_expr(m_int, get_bitwise_op(op), e1, e2);
}

static binop
get_arithmetic_op(arithmetic_op op)
{
  switch (op) {
  case op_add: return bo_add;
  case op_sub: return bo_sub;
  case op_mul: return bo_mul;
  case op_quo: return bo_quo;
  case op_rem: return bo_rem;
  }
}

expr* 
semantics::on_additive_expression(token tok, expr* e1, expr* e2)
{
  e1 = require_arithmetic(e1);
  e2 = require_arithmetic(e2);
  type* t = require_same(e1->get_type(), e2->get_type());
  
  arithmetic_op op = tok.get_arithmetic_operator();
  return new binop_expr(t, get_arithmetic_op(op), e1, e2);
}

expr* 
semantics::on_multiplicative_expression(token tok, expr* e1, expr* e2)
{
  e1 = require_arithmetic(e1);
  e2 = require_arithmetic(e2);
  type* t = require_same(e1->get_type(), e2->get_type());
  
  arithmetic_op op = tok.get_arithmetic_operator();
  return new binop_expr(t, get_arithmetic_op(op), e1, e2);
}

expr* 
semantics::on_cast_expression(expr* e, type* t)
{
  return new cast_expr(convert_to_type(e, t), t);
}

static unop
get_unary_op(token tok)
{
  switch (tok.get_name()) {
  case tok_arithmetic_operator:
    if (tok.get_arithmetic_operator() == op_add)
      return uo_pos;
    else if (tok.get_arithmetic_operator() == op_sub)
      return uo_neg;
    else if (tok.get_arithmetic_operator() == op_mul)
      return uo_deref;
    else
      throw std::logic_error("invalid operator");
  case tok_bitwise_operator:
    if (tok.get_bitwise_operator() == op_not)
      return uo_cmp;
    else if (tok.get_bitwise_operator() == op_and)
      return uo_addr;
    else
      throw std::logic_error("invalid operator");
  case tok_logical_operator:
    if (tok.get_logical_operator())
      return uo_not;
    else
      throw std::logic_error("invalid operator");
  default:
    throw std::logic_error("invalid token");
  }
}

expr* 
semantics::on_unary_expression(token tok, expr* e)
{
  unop op = get_unary_op(tok);
  type* t;
  switch (op) {
  case uo_pos:
  case uo_neg:
    e = require_arithmetic(e);
    t = e->get_type();
    break;

  case uo_cmp:
    e = require_integer(e);
    t = m_int;
    break;

  case uo_not:
    e = require_boolean(e);
    t = m_bool;
    break;

  case uo_addr:
    e = require_reference(e);
    t = new ptr_type(e->get_type());
    break;
  
  case uo_deref:
    e = require_pointer(e);
    t = static_cast<ptr_type*>(e->get_type())->get_element_type();
  }
  return new unop_expr(t, op, e); 
}

expr* 
semantics::on_call_expression(expr* e, const expr_list& args)
{
  e = require_function(e);
  fn_type* t = static_cast<fn_type*>(e->get_type());

  // Check the parameter length matches.
  type_list& parms = t->get_parameter_types();
  if (parms.size() < args.size())
    throw std::runtime_error("too many arguments");
  if (args.size() < parms.size())
    throw std::runtime_error("too few arguments");

  // The type of each argument is converted to the corresponding type.
  expr_list conv(args.size());
  for (std::size_t i = 0; i != parms.size(); ++i) 
    conv[i] = convert_to_type(args[i], parms[i]);

  return new call_expr(t->get_return_type(), e, conv);
}

expr* 
semantics::on_index_expression(expr* e, const expr_list& args)
{
  throw std::runtime_error("not implemented");
}

expr*
semantics::on_integer_literal(token tok)
{
  int val = tok.get_integer();
  return new int_expr(m_int, val);
}

expr*
semantics::on_boolean_literal(token tok)
{
  int val = tok.get_integer();
  return new bool_expr(m_bool, val);
}

expr*
semantics::on_float_literal(token tok)
{
  int val = tok.get_integer();
  return new float_expr(m_float, val);
}

expr*
semantics::on_id_expression(token tok)
{
  // assert(tok.is_identifier());
  symbol sym = tok.get_identifier();

  // Lookup the symbol.
  decl* d = lookup(sym);
  if (!d) {
    std::stringstream ss;
    ss << "no matching declaration for '" << *sym << "'";
    throw std::runtime_error(ss.str());
  }

  // The type depends on the declaration.
  type* t;
  typed_decl* td = dynamic_cast<typed_decl*>(d);
  if (td->is_variable())
    t = new ref_type(td->get_type());
  else
    t = td->get_type();

  return new id_expr(t, d);
}

// -------------------------------------------------------------------------- //
// Statements

stmt*
semantics::on_block_statement(const stmt_list& ss)
{
  return new block_stmt(ss);
}

void
semantics::start_block()
{
  // In the outermost block of a function definition, parameters are
  // declared as local variables.
  scope* parent = get_current_scope()->parent;
  if (dynamic_cast<global_scope*>(parent)) {
    fn_decl* fn = get_current_function();
    for (decl* parm : fn->m_parms)
      declare(parm);
  }
}

void
semantics::finish_block()
{
}

stmt*
semantics::on_if_statement(expr* e, stmt* s1, stmt* s2)
{
  // The condition is converted to bool.
  e = convert_to_bool(e);

  return new if_stmt(e, s1, s2);
}

stmt*
semantics::on_while_statement(expr* e, stmt* s)
{
  // The condition is converted to bool.
  e = convert_to_bool(e);

  return new while_stmt(e, s);
}

stmt*
semantics::on_break_statement()
{
  return new break_stmt();
}

stmt*
semantics::on_continue_statement()
{
  return new cont_stmt();
}

stmt*
semantics::on_return_statement(expr* e)
{
  fn_decl* fn = get_current_function();

  // The return value is converted to the return type of the function.
  e = convert_to_type(e, fn->get_return_type());
  
  return new ret_stmt(e);
}

stmt*
semantics::on_declaration_statement(decl* d)
{
  return new decl_stmt(d);
}

stmt*
semantics::on_expression_statement(expr* e)
{
  return new expr_stmt(e);
}

// -------------------------------------------------------------------------- //
// Declarations

void
semantics::declare(decl* d)
{
  // Check the current scope for an existing declaration.
  scope* s = get_current_scope();
  if (s->lookup(d->get_name())) {
    std::stringstream ss;
    ss << "redeclaration of " << *d->get_name();
    throw std::runtime_error(ss.str());
  }
  s->declare(d->get_name(), d);
}

decl*
semantics::on_variable_declaration(token n, type* t)
{
  if (t->is_reference())
    throw std::runtime_error("cannot create a reference variable");
  decl* var = new var_decl(n.get_identifier(), t);
  declare(var);
  return var;
}

decl*
semantics::on_variable_definition(decl* d, expr* e)
{
  var_decl* var = static_cast<var_decl*>(d);

  // The expression is converted to the declared type of the variable.
  e = convert_to_type(e, var->get_type());

  var->set_init(e);
  return var;
}

decl*
semantics::on_constant_declaration(token n, type* t)
{
  decl* var = new const_decl(n.get_identifier(), t);
  declare(var);
  return var;
}

decl*
semantics::on_constant_definition(decl* d, expr* e)
{
  const_decl* var = static_cast<const_decl*>(d);

  // The expression is converted to the declared type of the variable.
  e = convert_to_type(e, var->get_type());

  var->set_init(e);
  return var;
}

decl*
semantics::on_value_declaration(token n, type* t)
{
  decl* val = new value_decl(n.get_identifier(), t);
  declare(val);
  return val;
}

decl*
semantics::on_value_definition(decl* d, expr* e)
{
  value_decl* val = static_cast<value_decl*>(d);

  // The expression is converted to the declared type of the variable.
  e = convert_to_type(e, val->get_type());

  val->set_init(e);
  return val;
}

decl*
semantics::on_parameter_declaration(token n, type* t)
{
  decl* parm = new parm_decl(n.get_identifier(), t);
  declare(parm);
  return parm;
}

static type_list
get_parameter_types(const decl_list& parms)
{
  type_list types;
  for (const decl *d : parms)
    types.push_back(static_cast<const parm_decl*>(d)->get_type());
  return types;
}

decl*
semantics::on_function_declaration(token n, const decl_list& parms, type* ret)
{
  fn_type* ty = new fn_type(get_parameter_types(parms), ret);
  fn_decl* fn = new fn_decl(n.get_identifier(), ty, parms);
  fn->set_type(ty);
  declare(fn);

  assert(!m_fn);
  m_fn = fn;

  return fn;
}

decl*
semantics::on_function_definition(decl* d, stmt* s)
{
  fn_decl* fn = static_cast<fn_decl*>(d);
  fn->set_body(s);

  assert(m_fn == fn);
  m_fn = nullptr;

  return fn;
}

decl*
semantics::on_program(const decl_list& decls)
{
  return new prog_decl(decls);
}


// -------------------------------------------------------------------------- //
// Scope

void
semantics::enter_global_scope()
{
  assert(!m_scope);
  m_scope = new global_scope();
}

void
semantics::enter_parameter_scope()
{
  m_scope = new parameter_scope(m_scope);
}

void
semantics::enter_block_scope()
{
  m_scope = new block_scope(m_scope);
}

void
semantics::leave_scope()
{
  scope* cur = m_scope;
  m_scope = cur->parent;
  delete cur;
}

// -------------------------------------------------------------------------- //
// Lookup

decl*
semantics::lookup(symbol n)
{
  scope* s = get_current_scope();
  while (s) {
    if (decl* d = s->lookup(n))
      return d;
    s = s->parent;
  }
  return nullptr;
}

// -------------------------------------------------------------------------- //
// Type checking

expr*
semantics::require_reference(expr* e) 
{
  type* t = e->get_type();
  if (!t->is_reference())
    throw std::runtime_error("expected a reference");
  return e;
}

expr*
semantics::require_value(expr* e) 
{
  return convert_to_value(e);
}

expr*
semantics::require_arithmetic(expr* e)
{
  e = require_value(e);
  if (!e->is_arithmetic())
    throw std::runtime_error("expected an arithmetic expression");
  return e;
}

expr*
semantics::require_numeric(expr* e)
{
  e = require_value(e);
  if (!e->is_numeric())
    throw std::runtime_error("expected an arithmetic expression");
  return e;
}

expr*
semantics::require_scalar(expr* e)
{
  e = require_value(e);
  if (!e->is_scalar())
    throw std::runtime_error("expected a scalar expression");
  return e;
}

expr*
semantics::require_integer(expr* e)
{
  e = require_value(e);
  if (!e->is_int())
    throw std::runtime_error("expected an integer expression");
  return e;
}

expr*
semantics::require_boolean(expr* e)
{
  e = require_value(e);
  if (!e->is_bool())
    throw std::runtime_error("expected a boolean expression");
  return e;
}

expr*
semantics::require_function(expr* e)
{
  e = require_value(e);
  if (!e->is_function())
    throw std::runtime_error("expected a function");
  return e;
}

type*
semantics::require_same(type* t1, type* t2)
{
  if (!is_same_as(t1, t2))
    throw std::runtime_error("type mismatch");
  return t1;
}


type*
semantics::common_type(type* t1, type* t2)
{
  if (is_same_as(t1, t2))
    return t1;
  if (t1->is_reference_to(t2))
    return t2;
  if (t2->is_reference_to(t1))
    return t1;
  throw std::runtime_error("no common type");
}

expr*
semantics::require_pointer(expr* e) 
{
  e = require_value(e);
  type* t = e->get_type();
  if (!t->is_pointer())
    throw std::runtime_error("expected a pointer");
  return e;
}


// -------------------------------------------------------------------------- //
// Conversion

expr*
semantics::convert_to_value(expr* e)
{
  type* t = e->get_type();
  if (t->is_reference())
    return new conv_expr(e, conv_value, t->get_object_type());
  return e;
}

expr*
semantics::convert_to_bool(expr* e)
{
  e = convert_to_value(e);
  type* t = e->get_type();
  switch (t->get_kind()) {
  case type::bool_kind:
    return e;
  case type::char_kind:
  case type::int_kind:
  case type::float_kind:
  case type::ptr_kind:
  case type::fn_kind:
    return new conv_expr(e, conv_bool, m_bool);
  default:
    throw std::runtime_error("cannot convert to bool");
  }
}

expr*
semantics::convert_to_char(expr* e)
{
  e = convert_to_value(e);
  type* t = e->get_type();
  switch (t->get_kind()) {
  case type::char_kind:
    return e;
  case type::int_kind:
    return new conv_expr(e, conv_char, m_char);
  default:
    throw std::runtime_error("cannot convert to char");
  }
}

expr*
semantics::convert_to_int(expr* e)
{
  e = convert_to_value(e);
  type* t = e->get_type();
  switch (t->get_kind()) {
  case type::bool_kind:
  case type::char_kind:
    return new conv_expr(e, conv_int, m_int);
  case type::int_kind:
    return e;
  case type::float_kind:
    return new conv_expr(e, conv_trunc, m_int);
  case type::ptr_kind:
  case type::fn_kind:
  default:
    throw std::runtime_error("cannot convert to int");
  }
}

expr*
semantics::convert_to_float(expr* e)
{
  e = convert_to_value(e);
  type* t = e->get_type();
  switch (t->get_kind()) {
  case type::int_kind:
    return new conv_expr(e, conv_ext, m_float);
  case type::float_kind:
    return e;
  default:
    throw std::runtime_error("cannot convert to float");
  }
}

expr*
semantics::convert_to_type(expr* e, type* t)
{
  // Possibly apply a value conversion.
  if (t->is_object())
    e = convert_to_value(e);
  
  // If we've converted the type, stop.
  if (e->has_type(t))
    return e;

  // Otherwise, search for a conversion.  
  switch (t->get_kind()) {
  case type::bool_kind:
    return convert_to_bool(e);
  case type::char_kind:
    return convert_to_char(e);
  case type::int_kind:
    return convert_to_int(e);
  case type::float_kind:
    return convert_to_float(e);
  case type::ptr_kind:
    return require_pointer(e);
  case type::ref_kind:
    return require_reference(e);
  default:
    throw std::runtime_error("cannot convert to type");
  }
}
