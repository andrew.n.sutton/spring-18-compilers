#pragma once

#include "symbol.hpp"
#include "location.hpp"

#include <cassert>

enum token_name
{
  // Miscellaneous
  tok_eof,

  // Punctuators
  tok_left_brace,
  tok_right_brace,
  tok_left_paren,
  tok_right_paren,
  tok_left_bracket,
  tok_right_bracket,
  tok_comma,
  tok_semicolon,
  tok_colon,

  // Operators
  tok_relational_operator, // ==, !=, <, >, <=, >=
  tok_arithmetic_operator, // +, -, *, /, %
  tok_bitwise_operator, // &, |, ^, ~
  tok_logical_operator, // and, or, not
  tok_conditional_operator, // ?
  tok_assignment_operator, // =
  tok_arrow_operator, // ->

  // Keywords
  kw_as,
  kw_break,
  kw_continue,
  kw_def,
  kw_else,
  kw_if,
  kw_let,
  kw_var,
  kw_return,
  kw_while,

  tok_identifier,
  tok_binary_integer,
  tok_decimal_integer,
  tok_hexadecimal_integer,
  tok_boolean,
  tok_floating_point,
  tok_character,
  tok_string,
  tok_type_specifier,
};

const char* to_string(token_name n);
const char* get_spelling(token_name n);

// Kinds of relational operator
enum relational_op
{
  op_eq,
  op_ne,
  op_lt,
  op_gt,
  op_le,
  op_ge,
};

enum arithmetic_op
{
  op_add,
  op_sub,
  op_mul,
  op_quo,
  op_rem,
};

enum bitwise_op
{
  op_and,
  op_ior,
  op_xor,
  op_shl,
  op_shr,
  op_not,
};

enum logical_op
{
  logical_and,
  logical_or,
  logical_not,
};

enum pointer_op
{
  op_addr,
  op_deref,
};

enum type_spec
{
  ts_char,
  ts_bool,
  ts_int,
  ts_float,
};

const char* to_string(relational_op op);
const char* to_string(arithmetic_op op);
const char* to_string(bitwise_op op);
const char* to_string(logical_op op);
const char* to_string(type_spec ts);

enum radix
{
  binary = 2,
  decimal = 10,
  hexadecimal = 16,
};

/// Stored information about integer values.
struct integer_attr
{
  radix rad;
  long long value;
};

/// Stored information about strings. Note that string literals are stored in 
/// the symbol table so we can avoid copying. This class primarily lets us
/// different identifiers (also symbols) from strings when constructing tokens.
///
/// \todo Is there anything else we might want to put in here? Encoding?
struct string_attr
{
  symbol sym;
};


/// The union of all token attributes. The actual value is discriminated the
/// token name.
///
/// Note that 
union token_attr
{
  token_attr() = default;
  token_attr(symbol sym) : sym(sym) { }
  token_attr(relational_op op) : relop(op) { }
  token_attr(arithmetic_op op) : arithop(op) { }
  token_attr(bitwise_op op) : bitop(op) { }
  token_attr(logical_op op) : logicop(op) { }
  token_attr(integer_attr val) : intval(val) { }
  token_attr(double val) : fpval(val) { }
  token_attr(bool tf) : truthval(tf) { }
  token_attr(char c) : charval(c) { }
  token_attr(string_attr s) : strval(s) { }
  token_attr(type_spec ts) : ts(ts) { }

  symbol sym;
  relational_op relop;
  arithmetic_op arithop;
  bitwise_op bitop;
  logical_op logicop;
  integer_attr intval;
  double fpval;
  bool truthval;
  char charval;
  string_attr strval;
  type_spec ts;
};


/// A token is the name of a symbol and optional attribute. The kind
/// of attribute is determined by the name of the symbol.
class token
{
public:
  token();
  token(token_name n, location loc = {});
  token(token_name n, token_attr attr, location loc = {});
  token(symbol sym, location loc = {});
  token(relational_op op, location loc = {});
  token(arithmetic_op op, location loc = {});
  token(bitwise_op op, location loc = {});
  token(logical_op op, location loc = {});
  token(long long val, location loc = {});
  token(radix rad, long long val, location loc = {});
  token(token_name n, radix rad, long long val, location loc = {});
  token(double val, location loc = {});
  token(bool tf, location loc = {});
  token(char c, location loc = {});
  token(string_attr s, location loc = {});
  token(type_spec ts, location loc = {});

  /// Converts to true if this is not the end of file.
  operator bool() const { return m_name != tok_eof; }

  /// Returns the token name.
  token_name get_name() const { return m_name; }

  /// Returns the token attribute.
  token_attr get_attribute() const { return m_attr; }

  /// Returns the source location of the token.
  location get_location() const { return m_loc; }

  // Queries

  /// Returns true if this is an identifier.
  bool is_identifier() const { return m_name == tok_identifier; }

  /// Returns true if this is any integer token.
  bool is_integer() const;

  /// Returns true if this is a floating point token.
  bool is_floating_point() const;

  // Accessors

  /// Returns the symbol for an identifier token.
  symbol get_identifier() const;
  
  /// Returns the operator for a relational operator token.
  relational_op get_relational_operator() const;

  /// Returns the operator for a arithmetic operator token.
  arithmetic_op get_arithmetic_operator() const;
  
  /// Returns the operator for a bitwise operator token.
  bitwise_op get_bitwise_operator() const;
  
  /// Returns the operator for a logical operator token.
  logical_op get_logical_operator() const;
  
  /// Returns the value of an integer token.
  long long get_integer() const;

  /// Returns the value of a floating point token.
  double get_floating_point() const;

  /// Returns the radix of an integer or floating point token.
  radix get_radix() const;

  /// Returns the truth value of a boolean token.
  bool get_boolean() const;

  /// Returns the value of a character token.
  char get_character() const;

  /// Returns the value of a string token. 
  const std::string& get_string() const;

  /// Returns the type of a type specifier.
  type_spec get_type_specifier() const;

private:
  token_name m_name;
  token_attr m_attr;
  location m_loc;
};

inline bool
token::is_integer() const
{
  return tok_binary_integer <= m_name && m_name <= tok_hexadecimal_integer;
}

inline bool
token::is_floating_point() const
{
  return m_name == tok_floating_point;
}

inline symbol
token::get_identifier() const
{
  assert(m_name == tok_identifier);
  return m_attr.sym;
}

inline relational_op
token::get_relational_operator() const
{
  assert(m_name == tok_relational_operator);
  return m_attr.relop;
}

inline arithmetic_op
token::get_arithmetic_operator() const
{
  assert(m_name == tok_arithmetic_operator);
  return m_attr.arithop;
}

inline bitwise_op
token::get_bitwise_operator() const
{
  assert(m_name == tok_bitwise_operator);
  return m_attr.bitop;
}

inline logical_op
token::get_logical_operator() const
{
  assert(m_name == tok_logical_operator);
  return m_attr.logicop;
}

inline long long
token::get_integer() const
{
  assert(is_integer());
  return m_attr.intval.value;
}

inline radix
token::get_radix() const
{
  assert(is_integer());
  return m_attr.intval.rad;
}

inline double
token::get_floating_point() const
{
  assert(m_name == tok_floating_point);
  return m_attr.fpval;
}

inline bool
token::get_boolean() const
{
  assert(m_name == tok_boolean);
  return m_attr.truthval;
}

inline char
token::get_character() const
{
  assert(m_name == tok_character);
  return m_attr.charval;
}

inline const std::string&
token::get_string() const
{
  assert(m_name == tok_string);
  return *m_attr.strval.sym;
}

inline type_spec
token::get_type_specifier() const
{
  assert(m_name == tok_type_specifier);
  return m_attr.ts;
}

std::ostream& operator<<(std::ostream& os, token tok);
