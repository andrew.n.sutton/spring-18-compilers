#include "file.hpp"
#include "lexer.hpp"
#include "parser.hpp"
#include "codegen.hpp"

#include <iostream>

extern "C" int 
main(int argc, char* argv[])
{
  file input(argv[1]);  
  symbol_table syms;

  // lexer lex(syms, input);
  // while (token tok = lex())
  //   std::cout << tok << '\n';

  parser p(syms, input);
  decl* tu = p.parse_program();
  // tu->debug();

  generate(tu);
}


