#pragma once

#include <vector>

/// Represents the set of all expressions.
class type
{
public:
  enum kind {
    bool_kind,
    char_kind,
    int_kind,
    float_kind,
    ptr_kind,
    ref_kind,
    fn_kind,
  };

protected:
  type(kind k)
    : m_kind(k)
  { }

public:
  virtual ~type() = default;

  kind get_kind() const { return m_kind; }

  /// Returns true if this is the type int.
  bool is_int() const { return m_kind == int_kind; }

  /// Returns true if this is the type bool.
  bool is_bool() const { return m_kind == bool_kind; }

  /// Returns true if this is the type bool.
  bool is_float() const { return m_kind == float_kind; }

  /// Returns true if this is the type char.
  bool is_char() const { return m_kind == char_kind; }
  
  /// Returns true if this is a reference type.
  bool is_reference() const { return m_kind == ref_kind; }

  // Returns true if this is a reference to t.
  bool is_reference_to(const type* t);

  /// Returns true if this is a pointer type.
  bool is_pointer() const { return m_kind == ptr_kind; }

  /// Returns true if this is a pointer to t.
  bool is_pointer_to(const type* t);

  /// Returns true if this is a function type.
  bool is_function() const { return m_kind == fn_kind; }
  
  /// Returns true if this an object type.
  bool is_object() const { return !is_reference(); }

  /// Returns true if this is an arithmetic type.
  bool is_arithmetic() const;

  /// Returns true if this is a numeric type.
  bool is_numeric() const;
  
  // FIXME: This is wrong.
  bool is_scalar() const { return true; }

  /// Returns this as an object type.
  type* get_object_type() const;

  void debug() const;

private:
  kind m_kind;
};


using type_list = std::vector<type*>;


struct bool_type : type
{
  bool_type()
    : type(bool_kind)
  { }
};


struct char_type : type
{
  char_type()
    : type(char_kind)
  { }
};

struct int_type : type
{
  int_type()
    : type(int_kind)
  { }
};

struct float_type : type
{
  float_type()
    : type(float_kind)
  { }
};

// Pointer types of the form T*.
struct ptr_type : type
{
  ptr_type(type* t)
    : type(ptr_kind), m_elem(t)
  { }

  type* get_element_type() const { return m_elem; }

  type* m_elem;
};

// Pointer types of the form T*.
struct ref_type : type
{
  ref_type(type* t)
    : type(ref_kind), m_elem(t)
  { }

  type* get_object_type() const { return m_elem; }

  type* m_elem;
};

// A function type.
struct fn_type : type
{
  fn_type(const type_list& ps, type* ret)
    : type(fn_kind), m_parms(ps), m_ret(ret)
  { }
  
  const type_list& get_parameter_types() const { return m_parms; }
  type_list& get_parameter_types() { return m_parms; }

  type* get_return_type() const { return m_ret; }

  type_list m_parms;
  type* m_ret;
};


// -------------------------------------------------------------------------- //
// Type equivalence

bool is_same_as(const type* t1, const type* t2);
