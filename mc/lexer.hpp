#pragma once

#include "token.hpp"

#include <unordered_map>

class file;

/// The lexer is responsible for transforming input characters into output
/// tokens. Note that whitespace is excluded from the output (i.e., it is
/// not significant to syntactic and semantic analysis).
class lexer
{
public:
  lexer(symbol_table& syms, const file& f);

  token operator()() { return scan(); }
  
  token scan();

  bool eof() const;

  char peek() const;
  char peek(int n) const;

private:
  char accept();
  void accept(int n);
  char ignore();

  // Skip functions -- advance past certain lexical constructs
  void skip_space();
  void skip_newline();
  void skip_comment();

  // Accept functions -- yield a token.
  token lex_punctuator(token_name n);
  token lex_relational_operator(int len, relational_op op);
  token lex_arithmetic_operator(arithmetic_op op);
  token lex_bitwise_operator(int len, bitwise_op op);
  token lex_conditional_operator();
  token lex_assignment_operator();
  token lex_arrow_operator();
  token lex_word();
  token lex_number();
  token lex_binary_number();
  token lex_hexadecimal_number();
  token lex_character();
  token lex_string();

  // Like a lex but yields an un-escaped character.
  char scan_escape_sequence();

private:
  /// Used to create symbols.
  symbol_table& m_syms;

  /// The current lex state.
  const char* m_first;
  const char* m_last;

  /// The current location.
  location m_current_loc;

  /// The location of the current token.
  location m_token_loc;

  /// Stores information about reserved words.
  std::unordered_map<symbol, token> m_reserved;
};